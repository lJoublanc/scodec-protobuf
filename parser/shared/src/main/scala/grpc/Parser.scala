package grpc

import scala.util.parsing.combinator._
import scala.meta._

/** Parses `*.proto` files, as per
  * [[https://developers.google.com/protocol-buffers/docs/reference/proto3-spec the IDL specification]].
  *
  * @param names
  *   How to convert member names. Default to camleCase.
  */
trait Parser extends RegexParsers { self =>

  lazy val codecs: Codecs = new Codecs {}

  /** Converts a name into camelCase. This can be overridden. */
  val xformName: String => String = { s: String =>
    val Array(h, t @ _*) = s split '_'
    h ++ t.map(_.capitalize).foldLeft("")(_ ++ _)
  }

  private def unimplemented = (_: Any) => q"""/* Unparsed expression */ {}"""

  case class WithCodec[P](codec: Parser[Term], stat: P)

  /** Comments are not specified in the BNF grammar, so we add them here. */
  override protected val whiteSpace = """(\s|//.*)+""".r

  private def octalDigit: Parser[String] = """[0-7]""".r

  private def hexDigit: Parser[String] = """[0-9]|[A-F]|[a-f]""".r

  ////////////////
  // Identifiers
  ////////////////

  // TODO: look into replacing all the xx.Name with just Name to simplify code
  def ident: Parser[Term.Name] =
    """\p{Alpha}\w*""".r map xformName map (s => Term.Name(s))

  /** @return Either a `Term.Select` or a `Term.Name`. */
  def fullIdent: Parser[Term] = rep1sep(ident, ".") map {
    case prefix :: suffix :: suffixes =>
      suffixes.foldLeft(Term.Select(prefix, suffix)) { (qual, name) =>
        Term.Select(qual, name)
      }
    case prefix :: Nil => prefix
    case Nil           => throw new java.lang.Error("fullIdent can't be non-empty")
  }

  protected def messageName: Parser[Type.Name] = ident map { case Term.Name(name) =>
    Type.Name(name)
  }

  protected def enumName: Parser[Type.Name] = ident map { case Term.Name(n) =>
    Type.Name(n)
  }

  protected def fieldName = ident

  protected def oneofName = ident

  protected def mapName = ident

  protected def serviceName: Parser[Type.Name] = ident map { case Term.Name(n) =>
    Type.Name(n)
  }

  protected def rpcName = ident

  def messageType: Parser[Type] =
    ".".? ~> { ident <~ "." }.* ~ messageName map {
      case Nil ~ n             => n
      case (prefix :: Nil) ~ n => Type.Select(prefix, n)
      case (p1 :: p2 :: ps) ~ n =>
        Type.Select(
          ps.foldLeft(Term.Select(p1, p2))((select, name) => Term.Select(select, name)),
          n
        )
    }

  // FIXME: in the spec the path can be absolute; as above `messageType`
  def enumType: Parser[Type.Name] =
    ".".? ~> { ident <~ "." }.* ~> enumName

  /** Integer literals */
  def intLit: Parser[Lit.Int] = decimalLit | octalLit | hexLit

  protected def decimalLit: Parser[Lit.Int] =
    """[1-9]\d*""".r map (d => Lit.Int(d.toInt))

  protected def octalLit: Parser[Lit.Int] =
    """0[0-7]*""".r map (o => Lit.Int(o.toInt))

  protected def hexLit: Parser[Lit.Int] =
    """0[xX]\p{XDigit}+""".r map (x => Lit.Int(x.toInt))

  /** Floating-point literals */
  def floatLit: Parser[Lit.Double] =
    (decimals.<~(".") ~ decimals.? ~ exponent.?).map { case norm ~ subnorm ~ exp =>
      val s = norm ++ subnorm.getOrElse("") ++ exp.getOrElse("")
      Lit.Double(s.toDouble)
    } |
      (decimals ~ exponent).map { case norm ~ exp =>
        Lit.Double((norm ++ exp).toDouble)
      } |
      ("." ~> decimals ~ exponent.?).map { case subnorm ~ exp =>
        val s = "." ++ subnorm ++ exp.getOrElse("")
        Lit.Double(s.toDouble)
      } |
      "inf".^^^(Lit.Double(Double.PositiveInfinity)) |
      "nan".^^^(Lit.Double(Double.NaN))

  protected def decimals: Parser[String] = """\d+""".r

  protected def exponent: Parser[String] =
    ("e" | "E") ~ ("+" | "-").? ~ decimals map { case e ~ sign ~ d =>
      e ++ sign.getOrElse("") ++ d
    }

  /** Boolean */
  def boolLit: Parser[Lit.Boolean] =
    """true|false""".r map (x => Lit.Boolean(x.toBoolean))

  /** String literals */
  def strLit: Parser[Lit.String] =
    ("'" ~> { charValue('\'') }.* <~ "'") |
      ('"'.toString ~> { charValue('"') }.* <~ '"'.toString) map { s =>
        Lit.String(new String(s.toArray))
      }

  /** @param escape The quote (' or ") used to delimit this string */
  protected def charValue(escape: Char): Parser[Char] =
    hexEscape | octEscape | charEscape | (("""[^\x00\n\\""" + escape + "]").r: Parser[
      String
    ]).map(_.head)

  protected def hexEscape: Parser[Char] =
    "\\" ~> ("x" | "X") ~> hexDigit ~ hexDigit map { case hex1 ~ hex2 =>
      (hex1.toInt * 16 + hex2.toInt).toChar
    }

  protected def octEscape: Parser[Char] =
    "\\" ~> octalDigit ~ octalDigit ~ octalDigit map { case o1 ~ o2 ~ o3 =>
      (o1.toInt * 8 * 8 + o2.toInt * 8 + o3.toInt).toChar
    }

  protected def charEscape: Parser[Char] =
    "\\" ~> ("a" | "b" | "f" | "n" | "r" | "t" | "v" | "\\" | "'" | '"'.toString) map (_.head)

  protected def quote: Parser[String] = "'" | '"'.toString

  /** EmptyStatement */
  def emptyStatement: Parser[Stat] = ";" ^^^ q"{} ;"

  /** Constant */
  def constant: Parser[Term] =
    fullIdent |
      ({ "-" | "+" }.? ~ intLit).map {
        case Some("-") ~ i => Lit.Int(-i.value)
        case _ ~ i         => i
      } |
      ({ "-" | "+" }.? ~ floatLit).map {
        case Some("-") ~ f => Lit.Double(format = "-" ++ f.format)
        case _ ~ f         => f
      } |
      strLit |
      boolLit

  /** Syntax
    *
    * The syntax statement is used to define the protobuf version.
    *
    * @example
    *   {{{syntax = "proto3";}}} //FIXME: this is wrong; would accept "proto3'
    * i.e. mismatched quotes.
    */
  def syntax: Parser[String] =
    "syntax" ~> "=" ~> quote ~> "proto3" <~ (quote ~ ";")

  /** Import Statement
    *
    * The import statement is used to import another .proto's definitions. TODO: currently this is
    * just being ignored.
    * @example
    *   {{{import public "other.proto";}}}
    */
  def `import` = "import" ~> { "weak" | "public" }.? ~> strLit <~ ";" ^^^ {
    q"""/* Unparsed import statement */ {} """
  }

  /** Package
    *
    * The package specifier can be used to prevent name clashes between protocol message types.
    *
    * @example
    *   {{{package foo.bar;}}}
    */
  def `package`: Parser[List[Stat] => Source] =
    "package" ~> fullIdent <~ ";" map {
      case n: Term.Name    => stats => source"package $n { ..$stats }"
      case ns: Term.Select => stats => source"package $ns { ..$stats }"
    }

  /** Option
    *
    * Options can be used in proto files, messages, enums and services. An option can be a protobuf
    * defined option or a custom option. For more information, see Options in the language guide.
    *
    * @example
    *   {{{option java_package = "com.example.foo";}}} TODO: parse this
    */
  def option: Parser[Stat] =
    ("option" ~> optionName <~ "=") ~ constant <~ ";" map unimplemented

  protected def optionName: Parser[String] =
    (ident | ("(" ~> fullIdent <~ ")")) ~ { "." ~> ident }.* map { case _ ~ names =>
      names.mkString(".")
    }

  /** Fields
    *
    * Fields are the basic elements of a protocol buffer message. Fields can be normal fields, oneof
    * fields, or map fields. A field has a type and field number.
    */
  def `type`: Parser[Type] =
    "double" ^^ toCamel |
      "float" ^^ toCamel |
      "int32" ^^^ t"Int" |
      "int64" ^^^ t"Long" |
      "uint32" ^^^ t"UInt" |
      "uint64" ^^^ t"ULong" |
      "sint32" ^^^ t"Int" |
      "sint64" ^^^ t"Long" |
      "fixed32" ^^^ t"Int" |
      "fixed64" ^^^ t"Long" |
      "sfixed32" ^^^ t"Int" |
      "sfixed64" ^^^ t"Long" |
      "bool" ^^^ t"Boolean" |
      "string" ^^ toCamel |
      "bytes" ^^^ t"scodec.bits.ByteVector" |
      messageType |
      enumType

  private val toCamel: String => Type.Name = s => Type.Name(s.capitalize)

  protected def fieldNumber = intLit

  /** Normal field
    *
    * Each field has type, name and field number. It may have field options.
    *
    * TODO: fieldOption TODO: handle index position
    * @example
    *   {{{foo.bar nested_message = 2; repeated int32 samples = 4 [packed=true];}}}
    * @return
    *   Parameters for a top level `case class`.
    */
  def field: Parser[Term.Param] =
    "repeated".? ~ `type` ~ fieldName.<~(
      "="
    ) ~ fieldNumber ~ ("[" ~> fieldOptions <~ "]").? <~ ";" map {
      case None ~ tpe ~ name ~ index ~ _ => param"$name: $tpe" // -> index
      case Some(_) ~ tpe ~ name ~ index ~ _ =>
        param"$name: List[$tpe]" // -> index
    }

  protected def fieldOptions = fieldOption ~ { "," ~> fieldOption }.*

  protected def fieldOption = optionName.<~("=") ~ constant

  /** Oneof and oneof field
    *
    * A oneof consists of oneof fields and a oneof name.
    * @example
    *   {{{oneof foo { string name = 4; SubMessage sub_message = 9; }}}}
    * @todo
    *   Currently this requires a manual import of `shapeless.{:+:, CNil}` for arity > 2.
    */
  def oneof: Parser[Term.Param] =
    "oneof" ~> oneofName ~ ("{" ~> {
      oneofField | emptyStatement
    }.+ <~ "}") map { case name ~ flds =>
      val coprod: Type =
        flds collect { case t: Type => t } match {
          case List(l, r) => t"Either[$l,$r]"
          case fs =>
            fs.foldRight[Type](t"CNil")(
              Type.ApplyInfix(_, Type.Name(":+:"), _)
            )
        }
      System.err.println(
        s"Warning: coproudct ${coprod} found - implicit codec " +
          "|not implemented yet.".stripMargin
      )
      param"""$name: $coprod"""
    }

  // TODO: process oneof field name/number/options
  protected def oneofField =
    `type` <~ fieldName.<~("=") <~ fieldNumber <~ {
      "[" ~> fieldOptions <~ "]"
    }.? <~ ";"

  /** Map field
    *
    * A map field has a key type, value type, name, and field number. The key type can be any
    * integral or string type.
    *
    * @example
    *   {{{map<string, Project> projects = 3;}}}
    */
  def mapField: Parser[Term.Param] =
    "map" ~> "<" ~> keyType.<~(",") ~ `type`.<~(">") ~ mapName.<~(
      "="
    ) ~ fieldNumber ~ ("[" ~> fieldOptions <~ "]").* <~ ";" map {
      case (k: Type.Name) ~ v ~ name ~ idx ~ opts =>
        param"$name: Map[$k, $v]"
    }

  protected def keyType: Parser[Type] =
    guard(
      "int32" | "int64" | "uint32" | "uint64" | "sint32" | "sint64" |
        "fixed32" | "fixed64" | "sfixed32" | "sfixed64" | "bool" | "string"
    ) ~> `type`

  /** Reserved
    *
    * Reserved statements declare a range of field numbers or field names that cannot be used in
    * this message.
    *
    * @example
    *   {{{reserved 2, 15, 9 to 11; reserved "foo", "bar";}}}
    * @note
    *   The grammar incorrectly states names are not quoted (see example).
    */
  def reserved: Parser[Stat] = "reserved" ~> (ranges | fieldNames) <~ ";" map {
    case Left(rng :: rngs)  => q"/* Ignoring parsed `reserved` range */ {} "
    case Right(fld :: flds) => q"/* Ignoring parsed `reserved` fields */ {} "
    case _ =>
      throw new java.lang.Error("Impossible: ranges/fields are non-empty")
  }

  protected def ranges: Parser[Left[List[Range], List[Term.Name]]] =
    rep1sep(range, ",") map Left.apply

  protected def range: Parser[Range] =
    intLit ~ { "to" ~> (intLit | "max".^^^(Lit.Int(Int.MaxValue))) }.? map {
      case start ~ Some(end) => start.value to end.value
      case start ~ None      => start.value to Int.MaxValue
    }

  protected def fieldNames: Parser[Right[List[Range], List[Term.Name]]] =
    rep1sep(quotedName, ",") map Right.apply

  // TODO: this is gruesome
  private def quotedName: Parser[Term.Name] =
    strLit flatMap { case Lit.String(s) =>
      parseAll(fieldName, s) match {
        case Success(term, _)    => success(term)
        case NoSuccess(err, rem) => failure(err)
      }
    }

  /////////////////////////
  // Top Level definitions
  /////////////////////////

  /** Enum definition
    *
    * The enum definition consists of a name and an enum body. The enum body can have options and
    * enum fields. Enum definitions must start with enum value zero.
    *
    * @example
    *   {{{ enum EnumAllowingAlias { option allow_alias = true; UNKNOWN = 0; STARTED = 1; RUNNING =
    *   2 [(custom_option) = "hello world"]; } }}}
    */
  def `enum`: Parser[Stat] = "enum" ~> enumName ~ enumBody map {
    case name ~ body => // TODO : move partialCodec generation out of here
      val tName = Term.Name(name.value)
      q"""object $tName extends Enumeration { asEnum : Enumeration =>
          type $name = Value
          implicit def partialCodec : PartialCodec.Aux[0, $name] = enum[asEnum.type]
          ..$body
      }
      import $tName._
      """
  }

  // TODO: allow option
  protected def enumBody: Parser[List[Stat]] =
    "{" ~> (option | enumField | emptyStatement).* <~ "}"

  protected def enumField: Parser[Stat] =
    ident.<~("=") ~ intLit ~ {
      "[" ~> (enumValueOption ~ { "," ~> enumValueOption }.*) <~ "]"
    }.? <~ ";" flatMap {
      case id ~ idx ~ None => // below can't quasi-unquote $id for some reason.
        success(Defn.Val(Nil, Pat.Var(id) :: Nil, None, q"Value($idx)"))
      case id ~ idx ~ Some(headOpt ~ opts) =>
        err("Unimplemented: oneof with options")
    }

  protected def enumValueOption: Parser[Stat] =
    optionName <~ "=" ~ constant map unimplemented

  // TODO: eventually would like to define these as Term.Def, so that top-level
  // elements are `trait`s, and then provide a mix-in that would change thes to
  // case-classes.

  /** Message definition
    *
    * A message consists of a message name and a message body. The message body can have fields,
    * nested enum definitions, nested message definitions, options, oneofs, map fields, and reserved
    * statements.
    *
    * @example
    *   {{{ message Outer { option (my_option).a = true; message Inner { // Level 2 int64 ival = 1;
    *   } map<int32, string> my_map = 2; } }}}
    */
  def message: Parser[Term.Block] = "message" ~> messageName ~ messageBody map { case name ~ body =>
    val (decls, fields) = body
    val (codecs, args) = fields.unzip { case a ~ b => a -> b }

    val messageCodec =
      q"implicit def partialCodec : PartialCodec.Aux[2, $name] = message(..$codecs).as[$name]"

    val tName = Term.Name(name.value)
    val imports = Option.when(decls.nonEmpty) { q"import $tName.{partialCodec => _, _}" }
    val allDecls = decls.flatMap { // message returns a Term.Block, so we need to flatten.
      case blk: Term.Block => blk.stats
      case stat: Stat      => List(stat)
    } :+ messageCodec

    if (fields.isEmpty)
      Term.Block(
        q"case object $tName { ..$allDecls }"
          :: imports.toList
      )
    else
      Term.Block(
        q"object $tName { ..$allDecls }"
          :: imports.toList
          ::: q"case class $name (..$args)"
          :: Nil
      )
  }

  protected def messageBody: Parser[(List[Stat], List[Term ~ Term.Param])] = {
    "{" ~> {
      guard(codecs.field) ~ field ^^ Right.apply |
        enum ^^ Left.apply |
        message ^^ Left.apply |
        option ^^ Left.apply |
        guard(codecs.oneof) ~ oneof ^^ Right.apply |
        guard(codecs.mapField) ~ mapField ^^ Right.apply |
        reserved ^^ Left.apply |
        emptyStatement ^^ Left.apply
    }.* <~ "}" map { _ partitionMap [Stat, Term ~ Term.Param] identity }
  }

  /** Service definition
    *
    * @example
    *   {{{service SearchService { rpc Search (SearchRequest) returns (SearchResponse);}}}
    */
  def service: Parser[Stat] =
    "service" ~> serviceName ~ {
      "{" ~> {
        /* option | */
        rpc /*| emptyStatement*/
      }.* <~ "}"
    } map { case name ~ body =>
      q"""
        case class $name[F[_]](..$body)
      """
    }

  protected def rpc: Parser[Term.Param] =
    "rpc" ~> rpcName.map(decapitalize) ~ {
      "(" ~> "stream".? ~ messageType <~ ")"
    }.<~("returns") ~ {
      "(" ~> "stream".? ~ messageType <~ ")"
    } ~ (("{" ~> { option | emptyStatement }.* <~ "}") | ";") map {
      case name ~ (None ~ tpeIn) ~ (None ~ tpeOut) ~ _ =>
        param"$name: $tpeIn => F[$tpeOut]"
      case name ~ (None ~ tpeIn) ~ (Some(_) ~ tpeOut) ~ _ =>
        param"$name: $tpeIn => fs2.Stream[F, $tpeOut]"
      case name ~ (Some(_) ~ tpeIn) ~ (None ~ tpeOut) ~ _ =>
        param"$name: fs2.Stream[F, $tpeIn] => F[$tpeOut]"
      case name ~ (Some(_) ~ tpeIn) ~ (Some(_) ~ tpeOut) ~ _ =>
        param"$name: fs2.Stream[F, $tpeIn] => fs2.Stream[F, $tpeOut]"
    }

  private def decapitalize(t: Term.Name): Term.Name =
    t.copy(value = Character.toLowerCase(t.value.charAt(0)) + t.value.substring(1))

  /** Proto file
    * {{{
    *   syntax ~> { `import` | `package` | option | topLevelDef | emptyStatement }.*
    * }}}
    * An example .proto file:
    * {{{
    *  syntax = "proto3";
    *  import public "other.proto";
    *  option java_package = "com.example.foo";
    *  enum EnumAllowingAlias {
    *    option allow_alias = true;
    *    UNKNOWN = 0;
    *    STARTED = 1;
    *    RUNNING = 2 [(custom_option) = "hello world"];
    *  }
    *  message outer {
    *    option (my_option).a = true;
    *    message inner {   // Level 2
    *      int64 ival = 1;
    *    }
    *    repeated inner inner_message = 2;
    *    EnumAllowingAlias enum_field =3;
    *    map<int32, string> my_map = 4;
    *  }
    * }}}
    */
  def proto: Parser[Source] =
    for {
      f <- syntax ~> `package`
      ss <- (`import` | option | topLevelDef | emptyStatement).*
      topLevelDefs = scalaImports :: ss flatMap {
        case b: Term.Block => b.stats // message returns block/List[Stat]
        case s: Stat       => List(s)
      } filter (_.isTopLevelStat)
    } yield f(topLevelDefs)

  protected val scalaImports = q"""
    import shapeless.{:+:, CNil}
    import shapeless.{::, HNil}
    import scodec.protocols.protobuf.v3._
    """

  def topLevelDef: Parser[Stat] = message | enum | service

  /** Parsers in here are duplicates of those in the outer class. These take care of building the
    * codec programmatically, whereas those in the outer class generate the scala types.
    */
  trait Codecs {
    def `type`: Parser[Term] =
      "int32" ^^ Term.Name.apply |
        "double" ^^ Term.Name.apply |
        "float" ^^ Term.Name.apply |
        "int32" ^^ Term.Name.apply |
        "int64" ^^ Term.Name.apply |
        "uint32" ^^ Term.Name.apply |
        "uint64" ^^ Term.Name.apply |
        "sint32" ^^ Term.Name.apply |
        "sint64" ^^ Term.Name.apply |
        "fixed32" ^^ Term.Name.apply |
        "fixed64" ^^ Term.Name.apply |
        "sfixed32" ^^ Term.Name.apply |
        "sfixed64" ^^ Term.Name.apply |
        "bool" ^^ Term.Name.apply |
        "string" ^^ Term.Name.apply |
        "bytes" ^^ Term.Name.apply |
        messageType
    // TODO: enum - note that this is currently implemented in super.enum!

    def messageType: Parser[Term] =
      ".".? ~> { ident <~ "." }.* ~ ident map {
        case Nil ~ n             => q"$n.partialCodec"
        case (prefix :: Nil) ~ n => q"$prefix.$n.partialCodec"
        case (p1 :: p2 :: ps) ~ n =>
          Term.Select(
            Term.Select(
              ps.foldLeft(Term.Select(p1, p2))((select, name) => Term.Select(select, name)),
              n
            ),
            Term.Name("partialCodec")
          )
      }

    def field: Parser[Term] = {
      "repeated".? ~ `type`
        .<~(fieldName)
        .<~("=") ~ fieldNumber ~ ("[" ~> fieldOptions <~ "]").? <~ ";" map {
        case None ~ codec ~ idx ~ _maybeOpts =>
          q"$codec := $idx"
        case Some(
              _
            ) ~ codec ~ idx ~ _maybeOpts => // TODO: better parsing of packed
          q"repeated($codec, packed = false) := $idx"
      }
    }

    def oneof: Parser[Term] = {
      def oneofField: Parser[Term] =
        `type`.<~(fieldName).<~("=") ~ fieldNumber <~ {
          "[" ~> fieldOptions <~ "]"
        }.? <~ ";" map { case codec ~ idx =>
          q"$codec := $idx"
        }

      "oneof" ~> oneofName ~ ("{" ~> {
        oneofField /* | FIXME emptyStatement */
      }.+ <~ "}") map { case name ~ flds =>
        if (flds.length > 22) {
          val coprod = flds.foldRight[Term](Term.Name("HNil")) { case (next, tail) =>
            Term.ApplyInfix(next, Term.Name("::"), Nil, List(tail))
          }
          q"OneOfCodec($coprod)"
        } else q"oneOf(..$flds)"
      }
    }

    def mapField: Parser[Term] = {
      def keyType: Parser[Term] = guard(
        "int32" | "int64" | "uint32" | "uint64" | "sint32" | "sint64" |
          "fixed32" | "fixed64" | "sfixed32" | "sfixed64" | "bool" | "string"
      ) ~> `type`

      "map" ~> "<" ~> keyType.<~(",") ~ `type`.<~(">") ~ mapName.<~(
        "="
      ) ~ fieldNumber ~ ("[" ~> fieldOptions <~ "]").* <~ ";" map {
        case k ~ v ~ _name ~ idx ~ opts => q"mapField($k, $v) := $idx"
      }
    }
  }
}

object Parser extends Parser
