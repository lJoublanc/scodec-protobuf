package grpc

import org.scalatest.{Matchers, FlatSpec}
import scala.meta._

class ParserSpec extends FlatSpec with Matchers {
  lazy val parser = new Parser {}

  import parser._

  "strLit" should "parse double-quoted string" in {
    val s = { '"' +: "testing" } :+ '"'
    val tree = parseAll(strLit, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q""" "testing" """.structure
  }

  "package" should "parse package statement" in {
    val s = """package com.digitalasset.ledger.api.v1;"""
    val tree =
      parseAll(`package`, s) map { f =>
        f(Nil)
      }

    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual
      source"""package com.digitalasset.ledger.api.v1 {}""".structure
  }

  "reserved" should "parse ranges" in {
    val s = "reserved 2, 15, 9 to 11;"
    val tree = parseAll(reserved, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q"/* some comment */ {}".structure
  }

  it should "parse names" in {
    val s = """reserved "foo", "bar";"""
    val tree = parseAll(reserved, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q"/* some comment */ {}".structure
  }

  "mapField" should "parse a simple map" in {
    val s = "map<string, Project> projects = 3;"
    val tree = parseAll(mapField, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual param"projects: Map[String, Project]".structure
  }

  "message" should "parse different types of non-recursive fields" in {
    val m = """
      message Test1 {
        int32 foo = 1; 
        string bar = 2; 
        uint64 baz = 3; 
        bytes faz = 4;
      }
      """
    val tree = parseAll(parser.message, m)
    tree should not matchPattern { case _: Failure => }
    // System.err.println(tree.get.syntax)
    tree.get.structure shouldEqual q"""
      object Test1 {
        implicit def partialCodec : PartialCodec.Aux[2, Test1] = 
          message(
            int32 := 1,
            string := 2,
            uint64 := 3,
            bytes := 4
          ).as[Test1]
      }
      case class Test1(
        foo: Int,
        bar: String,
        baz: ULong,
        faz: scodec.bits.ByteVector
      )
    """.structure
  }

  // TODO: zero-field message

  it should "parse fully qualified names and repeated fields" in {
    val s = """
			// A composite command that groups multiple commands together.
			message Commands {
				google.protobuf.Timestamp ledger_effective_time = 6;
				google.protobuf.Timestamp maximum_record_time = 7;
				repeated Command commands = 8;
			}
		"""
    val tree = parse(parser.message, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q"""
      object Commands {
        implicit def partialCodec : PartialCodec.Aux[2, Commands] =
          message(
            google.protobuf.Timestamp.partialCodec := 6,
            google.protobuf.Timestamp.partialCodec := 7,
            repeated(Command.partialCodec, packed = false) := 8
            ).as[Commands]
      }
			case class Commands(
					ledgerEffectiveTime: google.protobuf.Timestamp,
					maximumRecordTime: google.protobuf.Timestamp,
					commands: List[Command]
			)
	  """.structure
  }

  "oneof" should "parse two-element coproduct" in {
    val s = """
		oneof foo {
    string name = 4;
    SubMessage sub_message = 9;
		}
		"""
    val tree = parseAll(oneof, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual param"""foo: Either[String,SubMessage]""".structure
  }

  it should "parse three-element coproduct" in {
    val s = """
		oneof foo {
    string name = 4;
    SubMessage sub_message = 9;
    int32 three = 3;
		}
		"""
    val tree = parseAll(oneof, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual param"""foo: String :+: SubMessage :+: Int :+: CNil""".structure
  }

  "enum" should "parse" in {
    val s = """
			 enum EnumAllowingAlias {
				 // option allow_alias = true;
				 UNKNOWN = 0;
				 STARTED = 1;
				 RUNNING = 2; //[(custom_option) = "hello world"];
			 }
		 """
    val tree = parseAll(enum, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q"""
       object EnumAllowingAlias extends Enumeration { asEnum : Enumeration =>
         type EnumAllowingAlias = Value
         implicit def partialCodec : PartialCodec.Aux[0, EnumAllowingAlias] = enum[asEnum.type]
				 val UNKNOWN = Value(0)
				 val STARTED = Value(1)
				 val RUNNING = Value(2)
       }
       import EnumAllowingAlias._
		 """.structure
  }

  "service" should "parse simple non-streaming service" in {
    val s = """
			service SearchService {
				rpc Search (SearchRequest) returns (SearchResponse);
			}
    """
    val tree = parseAll(service, s)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual q"""
      case class SearchService[F[_]](
        search: SearchRequest => F[SearchResponse] 
      )
    """.structure
  }

  "proto" should "parse example from BNF spec" in {
    val x = """
      syntax = "proto3";
      package com.example.foo; //added this to allow test to pass
      import public "other.proto";
      option java_package = "com.example.foo";
      enum EnumAllowingAlias {
        //option allow_alias = true;
        UNKNOWN = 0;
        STARTED = 1;
        RUNNING = 2; // [(custom_option) = "hello world"];
      }
      message outer {
        //option (my_option).a = true;
        message inner {   // Level 2
          int64 ival = 1;
        }
        repeated inner inner_message = 2;
        EnumAllowingAlias enum_field =3;
        map<int32, string> my_map = 4;
      }
    """

    val tree = parseAll(proto, x)
    tree should not matchPattern { case _: Failure => }
    tree.get.structure shouldEqual source"""
      package com.example.foo {
        import shapeless.{:+:, CNil}
        import shapeless.{::, HNil}
        import scodec.protocols.protobuf.v3._

        object EnumAllowingAlias extends Enumeration { asEnum : Enumeration =>
          type EnumAllowingAlias = Value
          implicit def partialCodec : PartialCodec.Aux[0, EnumAllowingAlias] = enum[asEnum.type]
          val UNKNOWN = Value(0)
          val STARTED = Value(1)
          val RUNNING = Value(2)
        }
        import EnumAllowingAlias._
        object outer {
          object inner {
            implicit def partialCodec : PartialCodec.Aux[2, inner] =
              message(int64 := 1).as[inner]
          }
          case class inner(ival: Long)
          implicit def partialCodec : PartialCodec.Aux[2, outer] =
            message(
              repeated(inner.partialCodec, packed = false) := 2,
              EnumAllowingAlias.partialCodec := 3,
              mapField(int32, string) := 4
            ).as[outer]
        }
        import outer.{partialCodec => _, _ }
        case class outer(
          innerMessage: List[inner],
          enumField: EnumAllowingAlias,
          myMap: Map[Int, String]
        )
      }
    """.structure
  }
}
