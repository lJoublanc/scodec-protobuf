# Google Protocol Buffers for scodec

This project is available from Bintray
```scala
resolvers += Resolver.bintrayRepo("ljoublanc","maven")
libraryDependencies += "com.joublanc" %% "scodec-protobuf" % $VERSION
```
Replacing `$VERSION` with the desired version. Alternatively you can depend on individual submodules:
  * `scodec-protobuf-codecs` codec instances
  * `scodec-protobuf-grpc` [fs2](https://github.com/functional-streams-for-scala/fs2)-based implementation of protobuf services
  * `scodec-protobuf-parser` parse `*.proto` files

Currently all documentation is in scaladoc. Clone this repo and run `sbt doc` to generate it - the target dir is mentioned in the command output.
