name := "scodec-protobuf"

description := "Google protocol buffers & gRPC"

enablePlugins(GitVersioning)

lazy val commonSettings = Seq(
  organization := "com.joublanc",
  scalaVersion := "2.13.10",
  licenses += "Apache-2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0"),
  libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test",
  scalacOptions ++= Seq(
    "-language:higherKinds",
    "-feature",
    "-Ywarn-unused:imports",
    "-Xlint",
    "-Ymacro-annotations",
    "-deprecation",
    "-Xlint:-byname-implicit", // See https://github.com/scala/bug/issues/12072
    "-Xlint:-adapted-args" // Used everywhere, especially message codec. See also https://github.com/scala/bug/issues/11110
  ),
  Compile / console/ scalacOptions ~=
    (_ diff Seq("-Ywarn-unused:imports", "-Xlint")),
  Test / console / scalacOptions ~=
    (_ diff Seq("-Ywarn-unused:imports", "-Xlint")),
  developers := List(
    Developer(
      "lJoublanc",
      "Luciano Joublanc",
      "luciano@joublanc.com",
      url("https://gitlab.com/ljoublanc")
    )
  )
)

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .aggregate(codecs.jvm, codecs.js, parser.jvm, grpc.jvm)
  .settings(
    publishArtifact := false
  )

lazy val codecs = crossProject(JSPlatform, JVMPlatform)
  .in(file("codecs"))
  .settings(commonSettings)
  .settings(
    name := "scodec-protobuf-codecs",
    description := "scodec codecs for protocol buffers",
    libraryDependencies += "org.scodec" %%% "scodec-core" % "1.11.7",
    console / initialCommands += """
        import scodec.protocols.protobuf.v3._
      """,
    consoleQuick / initialCommands := """
        import scodec.Codec
        import scodec.codecs
        import shapeless._
      """,
  )

lazy val parser = crossProject(JVMPlatform)
  .in(file("parser"))
  .settings(commonSettings)
  .settings(
    name := "scodec-protobuf-parser",
    description := "parse *.proto IDL files into scala case classes",
    libraryDependencies ++= Seq(
      "org.scalameta" %% "scalameta" % "4.6.0",
      "org.scala-lang.modules" %% "scala-parser-combinators" % "2.1.1"
    ),
    console / initialCommands += """
        import grpc.Parser._
      """,
    consoleQuick / initialCommands := """
        import scala.meta._
    """
  )

lazy val grpc = crossProject(JVMPlatform)
  .in(file("grpc"))
  .settings(commonSettings)
  .settings(
    name := "scodec-protobuf-grpc",
    description := "protobuf GRPC services using fs2",
    javacOptions ++= Seq("-target", "11"), // required by htttp-client dependency
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-okhttp-client" % "0.23.11",
      "com.squareup.okhttp3" % "okhttp-tls" % "4.9.3", //TODO: make this optional?
      "co.fs2" %% "fs2-scodec" % "3.4.0"
    ),
    consoleQuick / initialCommands := """
        import fs2.Stream
      """,
    console / initialCommands += """
        import cats.effect.IO
        implicit val cs = IO.contextShift(scala.concurrent.ExecutionContext.global)
        implicit val timer = IO.timer(scala.concurrent.ExecutionContext.global)
      """
  )
  .dependsOn(codecs)

/* The below is needed for CI on Gitlab/Kubernetes.
 * As this is a library, it is not mean to be run independently,
 * therefore this is a null task.
 */

val stage = taskKey[Unit]("Stage task")

val Stage = config("stage")

stage := {}
