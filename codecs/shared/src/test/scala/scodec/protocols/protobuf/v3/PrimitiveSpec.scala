package scodec.protocols.protobuf.v3

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers, Inside}

import scala.language.existentials
import scala.language.postfixOps

import scodec.DecodeResult
import scodec.Err.InsufficientBits
import scodec.TransformSyntax
import scodec.Attempt.{Successful, Failure}
import scodec.bits._
import scodec.protocols.protobuf.v3.{message => msg, oneOf => oneOF} // name clash with scalacheck

class PrimitiveSpec extends FlatSpec with Matchers with Inside with TableDrivenPropertyChecks {

  /** @see [[https://developers.google.com/protocol-buffers/docs/encoding#varints]] */
  val ints = Table[Int, BitVector](
    "decoded" -> "encoded",
    1 -> bin"0000 0001",
    300 -> bin"1010 1100 0000 0010"
  )

  /** @see [[https://developers.google.com/protocol-buffers/docs/encoding#simple]] */
  val prefixedInts = Table(
    "(idx, decoded)" -> "encoded",
    1 -> 150 -> hex"08 96 01".bits
  )

  val signedInts = Table[Long, Long](
    "signed" -> "unsigned",
    0L -> 0L,
    -1L -> 1L,
    1L -> 2L,
    -2L -> 3L,
    2147483647L -> 4294967294L,
    -2147483648L -> 4294967295L
  )

  "prefix" should "be a bijection" in {
    import ProtobufCodec.Prefix
    val (i, wireType) = (256, 2)
    inside(Prefix(i, wireType)) { case Prefix(Array(i2), Array(wireType2)) =>
      i shouldEqual i2
      wireType shouldEqual wireType2
    }
  }

  "int32" should "encode varints" in {
    forAll(ints) { (d, e) =>
      int32.force.encode(d) shouldEqual Successful(e)
    }
  }

  "sint64" should "encode known examples" in {
    forAll(signedInts) { (s, u) =>
      sint64.force.encode(s) shouldEqual int64.force.encode(u)
    }
  }

  it should "decode same values back" in {
    forAll(signedInts) { (s, _) =>
      Successful(s) shouldEqual
        sint64.force.decodeValue(sint64.force.encode(s).require)
    }
  }

  it should "decode varints" in {
    forAll(ints) { (d, e) =>
      int32.force.decode(e) shouldEqual Successful(
        DecodeResult(d, BitVector.empty)
      )
    }
  }

  it should "encode (incl prefix)" in {
    forAll(prefixedInts) { case ((i, d), e) =>
      { int32 := i }.encode(d) shouldEqual Successful(e)
    }
  }

  it should "decode (incl prefix)" in {
    forAll(prefixedInts) { case ((i, d), e) =>
      { int32 := i }.decode(e) shouldEqual Successful(
        DecodeResult(d, BitVector.empty)
      )
    }
  }

  "bool" should "be isomorphic to int32" in {
    (bool.force encode true) shouldEqual (int32.force encode 1)
    (bool.force encode false) shouldEqual (int32.force encode 0)
    (bool.force decodeValue (int32.force encode 1 require)).require shouldEqual true
    (bool.force decodeValue (int32.force encode 0 require)).require shouldEqual false
  }

  /** @see [[https://developers.google.com/protocol-buffers/docs/encoding#strings]] */
  val prefixedStrs = Table(
    "(idx, decoded)" -> "encoded",
    2 -> "testing" -> hex"12 07 74 65 73 74 69 6e 67".bits
  )

  "string" should "encode (incl prefix)" in {
    forAll(prefixedStrs) { case ((i, d), e) =>
      { string := i }
        .encode(d) shouldEqual Successful(e)
    }
  }

  it should "decode (incl prefix)" in {
    forAll(prefixedStrs) { case ((i, d), e) =>
      { string := i }
        .decode(e) shouldEqual Successful(DecodeResult(d, BitVector.empty))
    }
  }

  /* @see [[https://developers.google.com/protocol-buffers/docs/encoding#packed]] */
  "repeated" should "encode packed values" in {
    val test4 = {
      repeated(int32) := 4
    }
    test4.encode(3 :: 270 :: 86942 :: Nil) shouldEqual Successful(
      hex"22" ++ // key (field number 4, wire type 2)
        hex"06" ++ // payload size (6 bytes)
        hex"03" ++ // first element (varint 3)
        hex"8E 02" ++ // second element (varint 270)
        hex"9E A7 05" bits // third element (varint 86942)
    )
  }

  it should "encode unpacked values" in {
    val c = repeated(int32, packed = false) := 1
    val b = hex"08 96 01".bits
    c.encode(List.fill(3)(150)) shouldEqual Successful(b ++ b ++ b)
  }

  /** This is because in protobuf IDL it's impossible to nest repeated fields directly (You can do
    * so indirectly by wrapping in a `Message`, for instance). It's also not possible to implement a
    * (context-free) nested decoder; the inner list must complete successfully when it finds an
    * unknown index; however, this will appear as a 'success' for decoding a single element in the
    * outer codec, which will result in trying to call the nested codec again and again ("attempt to
    * decode next element").
    */
  it should "not allow nesting" in {
    "repeated(repeated(int32, packed = false), packed = false) := 1" shouldNot typeCheck
    "repeated(optional(int32), packed = false) := 1" shouldNot typeCheck
  }

  "optional" should "be isomorphic to a singleton unpacked list" in {
    val c = optional(int32) := 1
    val d = repeated(int32, packed = false) := 1
    c.encode(Some(150)) shouldEqual d.encode(List(150))
    c.encode(None) shouldEqual d.encode(List())
  }

  it should "handle trailing Nones" in {
    import scodec.Codec
    import scodec.protocols.protobuf.v3.implicits._
    case class Explicit(a: Int, b: Option[Int], c: Option[Long])
    case class Implicit(a: Int)

    val explicitBits = Codec.encode(Explicit(1, None, None))
    val implicitBits = Codec.encode(Implicit(1))
    explicitBits shouldEqual implicitBits
    Codec[Explicit].complete.decodeValue(implicitBits.require) shouldEqual
      Successful(Explicit(1, None, None))
  }

  it should "decode nested empty string" in {

    case class Foo(bar: Option[String])
    case class Baz(maybeFoo: Option[Foo])

    val c = msg(optional(msg(optional(string) := 1).as[Foo]) := 6).as[Baz].force

    withClue("inner string is defined: ") {
      val x = c.encode(Baz(Some(Foo(Some("bar"))))).require
      c.complete.decodeValue(x) shouldEqual Successful(Baz(Some(Foo(Some("bar")))))
    }
    withClue("inner string is empty: ") {
      val x = c.encode(Baz(Some(Foo(Some(""))))).require
      c.complete.decodeValue(x) shouldEqual Successful(Baz(Some(Foo(Some("")))))
    }
    withClue("inner option is empty: ") {
      val x = c.encode(Baz(Some(Foo(None)))).require
      x shouldEqual hex"3200".bits
      c.complete.decodeValue(x) shouldEqual Successful(Baz(Some(Foo(None))))
    }
    withClue("outer option is empty: ") {
      val x = c.encode(Baz(None)).require
      c.complete.decodeValue(x) shouldEqual Successful(Baz(None))
    }
  }

  it should "handle nesting repeated codecs" in {
    val in = msg(int32 := 2, int32 := 3) := 1
    val out = msg(optional(repeated(int32, packed = false)) := 2) := 1

    val bits = in.encode(0xffff, 0xaaaa).require

    out.complete.decodeValue(bits) shouldEqual Successful(Some(List(0xffff)))
  }

  "oneOf" should "encode and decode combination of int/string" in {
    forAll(prefixedInts) { case ((iIdx, iVal), iBits) =>
      forAll(prefixedStrs) { case ((sIdx, sVal), sBits) =>
        val c =
          scodec.protocols.protobuf.v3.oneOf(int32 := iIdx, string := sIdx)
        withClue("decode int") {
          (c decode iBits) shouldEqual Successful(
            DecodeResult(Left(iVal), BitVector.empty)
          )
        }
        withClue("decode string") {
          (c decode sBits) shouldEqual Successful(
            DecodeResult(Right(sVal), BitVector.empty)
          )
        }
        withClue("encode int") {
          (c encode Left(iVal)) shouldEqual (Successful(iBits))
        }
        withClue("encode string") {
          (c encode Right(sVal)) shouldEqual (Successful(sBits))
        }
      }
    }
  }

  "coprod" should "be identical to pure value in same idx pos" in {
    import shapeless._
    import shapeless.syntax.inject._
    import scodec.protocols.protobuf.v3.{oneOf => oneOF} // name clash

    type C = String :+: Int :+: Float :+: CNil

    val c = oneOF(string := 1, int32 := 2, float := 3)

    forAll(ints) { case (i, _) =>
      c encode i.inject[C] shouldEqual
        ({ int32 := 2 } encode i)
    }
  }

  /*
  it should "derive coproducts implicitly" in {
    import shapeless._
    import scodec.protocols.protobuf.v3.implicits._
    import shapeless.syntax.inject._

    /* Note the generic is ordered **alphabetically** !!! The order the
   * case classes are defined in source file does NOT affect this.
   */
    sealed trait Msg
    case class F(value: Float) extends Msg
    case class I(value: Int) extends Msg
    case class S(value: String) extends Msg
    case class Wrapper(msg: Msg)

    type C = F :+: I :+: S :+: CNil

    val c1 = OneOf[C].apply := 1
    val c2 = scodec.Codec[Wrapper]
    val f = 1.0f
    val i = 0xffff
    val s = "hello world"

    withClue("int32 failed") {
      (c1 encode I(i).inject[C]) shouldEqual (c2 encode Wrapper(I(i)))
    }

    withClue("string failed") {
      (c1 encode S(s).inject[C]) shouldEqual (c2 encode Wrapper(S(s)))
    }
    withClue("float failed") {
      (c1 encode F(f).inject[C]) shouldEqual (c2 encode Wrapper(F(f)))
    }
  }

  it should "derive recursive coproducts" in {
    import shapeless._
    import shapeless.syntax.inject._
    import scodec.Codec
    import scodec.codecs.{lazily, StringEnrichedWithCodecContextSupport}
    import scodec.protocols.protobuf.v3.implicits._
    type Sum = Inner :+: Int :+: CNil
    case class Outer(sum: Sum)
    object Outer {
      implicit def partialCodec: PartialCodec.Aux[Outer, 2] = msg {
        {
          { "inner" | { msg(lazily(Codec[Inner])) := 1 } } :+: // <-- Note the `lazily` to avoid stack-overflow.
            { "leaf" | { int32 := 2 } }
        }.choice.hlist.as[Outer]
      }
    }
    case class Inner(recurse: Outer)

    Codec encode Outer(1.inject[Sum])
    Codec encode Outer(Inner(Outer(1.inject[Sum])).inject[Sum])
  }
   */

  "enum" should "encode/decode subclasses of scala.Enum" in {
    object MyEnum extends Enumeration {
      val xs: Seq[Value] = prefixedInts.map { case ((idx, i), bits) =>
        Value(i)
      }
    }

    val myEnumCodec = enum[MyEnum.type]

    forAll(prefixedInts) { case ((idx, i), bits) =>
      val c = myEnumCodec := idx
      val e = MyEnum(i)
      (c encode e) shouldEqual Successful(bits)
      c.complete.decodeValue(bits) shouldEqual Successful(e)
    }
  }

  "mapField" should "be derived implicitly and be backward-compatible" in {
    import Combinators.MapFieldEntry
    import scodec.Codec
    import scodec.protocols.protobuf.v3.implicits._

    case class Project(id: Int)
    case class Msg(msg: Map[String, Project])
    case class Msg2(msg: List[MapFieldEntry[String, Project]])
    Codec.encode(Msg(Map("one" -> Project(135), "two" -> Project(1)))) shouldEqual
      Codec.encode(
        Msg2(
          List(
            MapFieldEntry("one", Project(135)),
            MapFieldEntry("two", Project(1))
          )
        )
      )
  }

  /* @see [[https://developers.google.com/protocol-buffers/docs/encoding#embedded]] */
  "message" should "encode embedded message" in {
    val test1 = msg(int32 := 1)
    val test3 = test1 := 3
    test3.encode(150) shouldEqual Successful(hex"1a 03 08 96 01".bits)
  }

  /* Note this differs from previous example as test3 idx is 1 (not 3) */
  it should "encode using implicit derivation" in {
    import scodec.Codec
    import scodec.protocols.protobuf.v3.implicits._

    case class Test1(a: Int)
    case class Test3(c: Test1)
    Codec.encode(Test3(Test1(150))) shouldEqual Successful(
      hex"0a 03 08 96 01".bits
    )
  }

  it should "encode non-contiguous indices correctly" in {
    import scodec.Codec
    import scodec.protocols.protobuf.v3.implicits._

    case class NonContiguous(a: Int, b: Int, c: Int)
    object NonContiguous {
      implicit def pc: PartialCodec.Aux[2, NonContiguous] =
        msg(int32 := 1, int32 := 3, int32 := 4).as[NonContiguous]
    }

    val x = NonContiguous(1, 2, 3)
    NonContiguous.pc.force.encode(x).require.toBin shouldEqual
      Codec.encode(x).require.toBin
  }

  it should "de/encode out of order indices" in {
    val bits = msg(int32 := 1, string := 3, int32 := 2).force
      .encode((1, "three", 2))
      .require
    msg(int32 := 1, int32 := 2, string := 3).force.complete
      .decodeValue(bits) shouldEqual Successful((1, 2, "three"))
    msg(int32 := 1, int32 := 2, string := 3, int32 := 4).force.complete
      .decodeValue(bits) should matchPattern { case Failure(_: InsufficientBits) =>
    }
    msg(int32 := 1, optional(int32) := 2, string := 3).force.complete
      .decodeValue(bits) shouldEqual Successful((1, Some(2), "three"))
  }

  it should "not stack-overflow on tail-recursive definitions" in {
    import scodec.TransformSyntax
    import scodec.protocols.protobuf.v3.implicits._

    case class First(i: Int, s: Second)
    case class Second(l: Long, e: Either[String, First])
    object Second {
      implicit def pc: PartialCodec.Aux[2, Second] =
        msg(int64 := 1, oneOF(string := 2, PartialCodec[First] := 3)).as[Second]
    }

    val pc = PartialCodec[First] := 1
    val x = First(1, Second(2L, Left("stop")))
    val bits = pc.encode(x)
    pc.complete.decodeValue(bits.require) shouldEqual Successful(x)
  }

  it should "ignore unknown indices for forward-compatibility" in {
    import scodec.protocols.protobuf.v3.JVMPrimitives

    val v2 =
      msg(int32 := 1, string := 2, uint32 := 3).force.encode((1, "two", JVMPrimitives.UInt(3)))
    msg(int32 := 1, string := 2).force.complete.decodeValue(v2.require) shouldEqual Successful(
      (1, "two")
    )
  }

  it should "not stack-overflow on eager recursion" in {
    import scodec.TransformSyntax
    import scodec.protocols.protobuf.v3.implicits._

    case class First(s: Second, i: Int)
    case class Second(e: Either[First, String], l: Long)
    object Second {
      implicit def pc: PartialCodec.Aux[2, Second] =
        msg(oneOF(PartialCodec[First] := 3, string := 2), int64 := 1).as[Second]
    }

    val pc = PartialCodec[First] := 1
    val x = First(Second(Right("stop"), 2L), 1)
    val bits = pc.encode(x)
    pc.complete.decodeValue(bits.require) shouldEqual Successful(x)
  }

  "error context" should "display correctly for primitive" in {
    inside((int32 := 1).decode(BitVector.empty)) { case Failure(err) =>
      err.context shouldEqual "1/int32".split("/").toList
    }
  }

  it should "display correctly for multi-field message" in {
    val bits = (msg(optional(int32) := 2, optional(float) := 3) := 1)
      .encode((None, None))
      .require
    inside((msg(int32 := 2, float := 3) := 1).decode(bits)) {
      // TODO(#29): currently the message codec builds the results with a right
      // fold, so errors are thrown in the wrong decoding order
      case Failure(err) => err.context shouldEqual "1/message/3/float".split("/").toList
    }
  }

  it should "display correctly for oneOf" in {
    val bits = (double := 2).encode(0xbeeffeed).require
    inside(oneOF(int32 := 1, float := 2).decode(bits)) { case Failure(err) =>
      err.context shouldEqual "oneOf/2/float".split("/").toList
    }

    // This is the case when an element is added to the sum, e.g. in a newer
    // version, but we have no way of knowing that.
    import shapeless._
    val bits2 = msg(oneOF(int32 := 1, double := 2, float := 3), string := 4).force
      .encode((Inr(Inl(0xbeeffeed))), "foo")
      .require
    inside(msg(oneOF(int32 := 1, float := 3), string := 4).force.decode(bits2)) {
      case Failure(err) => err.context shouldEqual "message/oneOf".split("/").toList
    }
  }

  it should "display correctly for nested message" in {
    val bits = (msg(msg(optional(int32) := 3) := 2) := 1)
      .encode(((Option.empty[Int])))
      .require
    inside((msg(msg(int32 := 3) := 2) := 1).decode(bits)) { case Failure(err) =>
      err.context shouldEqual "1/message/2/message/3/int32".split("/").toList
    }
  }

  it should "display correctly for transformed message partial codec" in {
    case class Foo(i: Int)
    object Foo {
      implicit def pc: PartialCodec.Aux[2, Foo] =
        msg(int32 := 2).as[Foo]
    }

    val bits = (msg(optional(int32) := 2) := 1).encode((None)).require
    inside((Foo.pc := 1).decode(bits)) { case Failure(err) =>
      err.context shouldEqual "1/message/2/int32".split("/").toList
    }
  }

  it should "display correctly for transformed nested message partial codec" in {
    case class Foo(bar: Bar)
    object Foo {
      implicit def pc: PartialCodec.Aux[2, Foo] =
        msg(Bar.pc := 2).as[Foo]
    }

    case class Bar(baz: Int)
    object Bar {
      implicit def pc: PartialCodec.Aux[2, Bar] =
        msg(int32 := 3).as[Bar]
    }

    val bits = (msg(msg(optional(int32) := 3) := 2) := 1)
      .encode(((Option.empty[Int])))
      .require
    inside((Foo.pc := 1).decode(bits)) { case Failure(err) =>
      err.context shouldEqual "1/message/2/message/3/int32".split("/").toList
    }
  }

  it should "display correctly for transformed protobuf codec" in {
    case class Foo(i: Int)

    val bits = (msg(optional(int32) := 2) := 1).encode((None)).require
    inside((msg(int32 := 2) := 1).as[Foo].decode(bits)) { case Failure(err) =>
      err.context shouldEqual "1/message/2/int32".split("/").toList
    }
  }

}
