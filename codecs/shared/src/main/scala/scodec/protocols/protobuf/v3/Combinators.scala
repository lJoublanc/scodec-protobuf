package scodec.protocols.protobuf.v3

import scodec.{Codec, Transformer, Err}
import shapeless.{=:!=, HNil, <:!<}

/** Combinators for embedded and repeated elements. */
trait Combinators { self: Primitives =>
  import Combinators.UnpackedRepeatedPartialCodec

  /** Allows a field to be missing when decoding, returning `None` in this case.
    * @note
    *   Implicit resolution is provided by [[Implicits#ImplicitPartialCodecs#implicitOneOf]]
    */
  def optional[W <: Int, A](
      field: => PartialCodec.Aux[W, A]
  ): PartialCodec.Aux[W, Option[A]] = new PartialCodec[Option[A]] {
    import scodec.Attempt._
    import scodec.{TransformSyntax, Err, Decoder, DecodeResult, Encoder}
    import scodec._
    import scodec.codecs._
    import scodec.bits.BitVector
    import ProtobufCodec.{Prefix, InvalidIndex}

    type WireType = W

    def force = field.force.widenOpt(Some(_), identity)

    def wireIncrement = 1

    def :=(
        index: Int
    )(implicit wireType: ValueOf[W]): ProtobufCodec[Option[A]] = {
      val c = field := index
      val d = new Decoder[Option[A]] {
        def decode(bits: BitVector): Attempt[DecodeResult[Option[A]]] = {
          if (bits.isEmpty)
            Successful(DecodeResult(None, bits))
          else
            c.decode(bits) match {
              case Failure(e: InvalidIndex) =>
                Successful(DecodeResult(None, bits))
              case Successful(DecodeResult(a, rem)) =>
                Successful(DecodeResult(Some(a), rem))
              case error: Failure => error
            }
        }
      }

      val e = Encoder((as: Option[A]) => Encoder.encodeSeq(c.asEncoder)(as.toSeq))

      val p = Prefix(index, valueOf[W])
      // using `provide` is quite hacky, but the only way I can think of not
      // triggering `InvalidIndex` error.
      ProtobufCodec(provide(p), p, Codec(e, d))
    }
  }

  /** Repeated, packed element. Only valid for elements with wire-type ≠ 2. */
  def repeated[W <: Int, A](field: PartialCodec.Aux[W, A], packed: true = true)(implicit
      isScalar: W =:!= 2
  ): PartialCodec.Aux[2, List[A]] = {
    val _ = isScalar
    PartialCodec.LengthDelimited[List[A], 2](scodec.codecs.list(field.force))
  }

  /** Repeated, '''non'''-packed elements.
    * @note
    *   To use this codec, you '''must''' specify `packed = false`.
    */
  def repeated[W <: Int, A](
      field: => PartialCodec.Aux[W, A],
      packed: false
  )(implicit
      notNestedList: A <:!< List[_],
      notNestedOption: A <:!< Option[_]
  ): PartialCodec.Aux[W, List[A]] =
    new UnpackedRepeatedPartialCodec(field)

  /** Convenience syntax for message with a single element. */
  def message[A](codec: => ProtobufCodec[A])(implicit
      msgCodec: MessageCodec.Aux[shapeless.::[ProtobufCodec[A], HNil], shapeless.::[A, HNil]]
  ): PartialCodec.Aux[2, A] =
    PartialCodec
      .LengthDelimited(msgCodec(shapeless.::(codec, HNil)))
      .xmapc(_.head)(shapeless.::(_, HNil))

  /** Facilitates `.as[A]` syntax for tuples, returned by [[message]] */
  implicit def tupleTransformer[P <: Product, L <: shapeless.HList, A](implicit
      gen: shapeless.Generic.Aux[P, L],
      tuple: shapeless.Generic.Aux[A, L]
  ): Transformer[P, A] = new Transformer[P, A] {
    import scodec.TransformSyntax
    def apply[F[_]: scodec.Transform](fa: F[P]) =
      fa.xmap[A](a => tuple.from(gen.to(a)), p => gen.from(tuple.to(p)))
  }

  /** Decode multiple values, possibly out of order.
    *
    * Encoding is done in the order of arguments. So in the example above, `uint` (with index 3)
    * would be attempted ''before'' `string` (index 2).
    *
    * Decoding is attempted linearly, so for optimal performance, arguments should match the order
    * data was encoded with. This will depend on the encoder (which may not even be this library),
    * and '''not''' the *.proto specification.
    *
    * @param tuple
    *   A tuple of [[ProtobufCodec]]
    * @example
    *   {{{message(int32 := 1, uint32 := 3, string : = 2)}}}
    * @tparam P
    *   Tuple of Codec[*]
    * @tparam L
    *   HList of Codec[*]
    * @tparam M
    *   HList of *
    * @tparam A
    *   Tuple of *
    */
  def message[P, L <: shapeless.HList, M <: shapeless.HList, A](tuple: => P)(implicit
      gen: shapeless.Generic.Aux[P, L],
      msgCodec: MessageCodec.Aux[L, M],
      tupler: shapeless.ops.hlist.Tupler.Aux[M, A],
      xform: scodec.Transformer[M, A]
  ): PartialCodec.Aux[2, A] = {
    val _ = tupler // required evidence for xform
    PartialCodec.LengthDelimited(msgCodec(gen to tuple).as[A])
  }

  /** Encodes one of two values.
    * @note
    *   if you prefer to work with `shapeless.Coproduct`, use the the overload that takes a tuple
    *   instead.
    */
  def oneOf[A, B](
      first: ProtobufCodec[A],
      second: ProtobufCodec[B]
  ): ProtobufCodec[Either[A, B]] = {
    import shapeless.{Inl, Inr}
    oneOf((first, second)).xmapc {
      case Inl(a)      => Left[A, B](a)
      case Inr(Inl(b)) => Right[A, B](b)
      case _           => throw new MatchError("Impossible coprod -> either conversion")
    } {
      case Left(a)  => Inl(a)
      case Right(b) => Inr(Inl(b))
    }
  }

  /** @tparam P
    *   a tuple of [[ProtobufCodec]].
    * @tparam Q
    *   a coproduct aligned with `P`
    * @example{{{
    *   oneOf(string := 4, message(int32 := 1)) }}}
    */
  def oneOf[P, L <: shapeless.HList, Q <: shapeless.Coproduct](tuple: P)(implicit
      gen: shapeless.Generic.Aux[P, L],
      oneOfCodec: OneOfCodec.Aux[L, Q]
  ): ProtobufCodec[Q] = oneOfCodec(gen to tuple)

  /** @note This may be replaced with dotty `enum`s eventually. */
  def `enum`[E <: Enumeration](implicit
      E: ValueOf[E]
  ): PartialCodec.Aux[0, E.value.Value] =
    int32.narrow[E.value.Value](
      i => scodec.Attempt fromTry scala.util.Try(E.value(i)),
      _.id
    )

  import shapeless.Typeable
  def mapField[W: ValueOf, X: ValueOf, K: Typeable, V: Typeable](
      key: PartialCodec.Aux[W, K],
      value: PartialCodec.Aux[X, V]
  ): PartialCodec.Aux[2, Map[K, V]] = {
    repeated(message((key := 1, value := 2)), packed = false)
      .xmap[Map[K, V]](_.toMap, _.toList)
  }
}

object Combinators {

  case class MapFieldEntry[K, V](
      key_type: K,
      value_type: V
  )

  /** @note When decoding, repeated fields must be consecutive. */
  class UnpackedRepeatedPartialCodec[W <: Int, A](
      field: => PartialCodec.Aux[W, A]
  ) extends PartialCodec[List[A]] {
    import scodec._
    import scodec.codecs._
    import scodec.bits.BitVector
    import scodec.Attempt._
    import ProtobufCodec.{Prefix, InvalidIndex}

    import scala.annotation.tailrec

    type WireType = W

    protected[protobuf] def wireIncrement = 1 // repeated

    /** This implementation may not do what you expect. If you have n values, it will remove the
      * prefix from each one of those n values - not just the leading one.
      */
    def force = list(field.force)

    def :=(
        index: Int
    )(implicit wireType: ValueOf[W]): ProtobufCodec[List[A]] = {
      val c = field := index
      val d = new Decoder[List[A]] {
        def decode(bits: BitVector): Attempt[DecodeResult[List[A]]] = {
          @tailrec def f(
              bits: BitVector,
              l: List[A]
          ): Attempt[DecodeResult[List[A]]] =
            if (bits.isEmpty)
              Successful(DecodeResult(Nil, bits))
            else
              c.decode(bits) match {
                case Failure(e: InvalidIndex) =>
                  Successful(DecodeResult(l.reverse, bits))
                case Successful(DecodeResult(a, rem)) =>
                  if (rem.isEmpty) Successful(DecodeResult((a :: l).reverse, rem))
                  else f(rem, a :: l)
                case error => error.map(_ map (_ :: l))
              }
          f(bits, Nil)
        }
      }

      val e = list(c).asEncoder

      val p = Prefix(index, valueOf[W])
      // using `provide` is quite hacky, but the only way I can think of not
      // triggering `InvalidIndex` error.
      ProtobufCodec(provide(p), p, Codec(e, d))
    }
  }
}
