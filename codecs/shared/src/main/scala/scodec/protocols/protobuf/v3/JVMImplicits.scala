package scodec.protocols.protobuf.v3

import java.nio.charset.Charset

trait JVMImplicits extends Implicits { self: Primitives =>

  object implicits extends self.ImplicitPartialCodecs {

    type IntWireType = 0

    type LongWireType = 0

    type UIntWireType = 0

    type ULongWireType = 0

    implicit lazy val defaultCharset: Charset =
      java.nio.charset.StandardCharsets.UTF_8

    implicit def implicitInt: PartialCodec.Aux[IntWireType, Int] = self.int32

    implicit def implicitLong: PartialCodec.Aux[LongWireType, Long] = self.int64

    implicit def implicitUInt: PartialCodec.Aux[UIntWireType, UInt] =
      self.uint32

    implicit def implicitULong: PartialCodec.Aux[ULongWireType, ULong] =
      self.uint64

    implicit def implicitDouble: PartialCodec.Aux[1, Double] = self.double

    implicit def implicitFloat: PartialCodec.Aux[5, Float] = self.float

    implicit def implicitBool: PartialCodec.Aux[0, Boolean] = self.bool

    implicit def implicitString(implicit
        cs: Charset
    ): PartialCodec.Aux[2, String] =
      self.string(cs)

    implicit def implicitBytes: PartialCodec.Aux[2, scodec.bits.ByteVector] =
      self.bytes
  }
}
