package scodec.protocols.protobuf.v3

import scodec.{Codec, Transform, Transformer}
import shapeless.{HList, Generic, Lazy}
import ProtobufCodec.Prefix

/** Wrapper for codecs that are unframed, that is, missing index/wire-type prefix. This is not meant
  * to be used directly; you probably forgot to call the assignment operator [[PartialCodec#:=]], to
  * turn this into a fully-framed codec. e.g.
  * {{{
  * scala> int32 encode 300
  *              ^
  *        error: value encode is not a member of scodec.protocols.protobuf.v3.PartialCodec[Int(0),Int]
  * }}}
  * instead do
  * {{{
  * scala> { int32 := 4 } encode 300
  * res1: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(24 bits, 0x20ac02))
  * }}}
  *
  * For simple types, this is implemented as a value type so has minimum overhead.
  *
  * @tparam A
  *   The decoded type.
  * @tparam W
  *   The wire type, as an integer literal. Uninhabited.
  */
trait PartialCodec[A] extends Any { self =>

  /** The wire type, as defined in GRPC. */
  type WireType

  /** Integer used to determine the next element's index. This is used when implicitly deriving
    * codecs, in order to determine the next element.
    */
  protected[protobuf] def wireIncrement: Int

  /** Extracts the underlying codec, without the index/wire type framing. */
  protected[protobuf] def force: Codec[A]

  /** Pushes `context` onto any error messages. */
  final def withContext(context: String): PartialCodec.Aux[WireType, A] =
    new PartialCodec[A] {
      type WireType = self.WireType
      def wireIncrement = self.wireIncrement
      def force = self.force withContext context
      def :=(index: Int)(implicit wireType: ValueOf[WireType]) = {
        import scodec.bits.BitVector
        val c = self.:=(index)(wireType)

        new ProtobufCodec(c.prefixCodec, c.prefix, c.force) {
          override def decode(bits: BitVector) =
            c.decode(bits).mapErr(_.pushContext(context).pushContext(index.toString))
          override def encode(a: A) =
            c.encode(a).mapErr(_.pushContext(context).pushContext(index.toString))
        }
      }
    }

  /** Frames the partial codec with `index` and wire type, turning it into a full `Codec[_]`
    */
  def :=(index: Int)(implicit wireType: ValueOf[WireType]): ProtobufCodec[A]
}

object PartialCodec {

  type Aux[W, A] = PartialCodec[A] { type WireType = W }

  /** Implicit summoner. Not to be confused with constructor. */
  def apply[A](implicit tc: Lazy[PartialCodec[A]]): Aux[tc.value.WireType, A] =
    tc.value

  case class Single[W <: Int, A](payload: Codec[A]) extends AnyVal with PartialCodec[A] {

    def force = payload

    type WireType = W

    protected[protobuf] def wireIncrement: Int = 1

    def :=(index: Int)(implicit wireType: ValueOf[W]): ProtobufCodec[A] =
      ProtobufCodec(Prefix.codec, Prefix(index, wireType.value), payload)
  }

  /** Default constructor forwards to [[Single]] codec constructor. */
  def apply[W <: Int, A](payload: Codec[A]): PartialCodec.Aux[W, A] =
    Single(payload)

  /** Partial codec where the length is not known. Admits recursive definitions so the payload is
    * evaluated lazily to support recursive ADTs.
    */
  class LengthDelimited[W <: Int, A](payload: => Codec[A]) extends PartialCodec[A] {

    type WireType = W

    private lazy val p = payload // TODO: See scodec.codecs.lazy comment on thread contention

    def force = p

    protected[protobuf] def wireIncrement: Int = 1

    def :=(index: Int)(implicit wireType: ValueOf[W]): ProtobufCodec[A] =
      ProtobufCodec(
        Prefix.codec,
        Prefix(index, wireType.value),
        lengthDelimited(p)
      )
  }

  object LengthDelimited {
    def apply[A, W <: Int](payload: => Codec[A]): PartialCodec.Aux[W, A] =
      new LengthDelimited[W, A](payload)
  }

  /** Provides `scodec.TransformSyntax`, **preserving the wire type**. */
  implicit def invariantInstance[W <: Int]: Transform[({ type C[a] = PartialCodec.Aux[W, a] })#C] =
    new Transform[({ type C[a] = PartialCodec.Aux[W, a] })#C] {
      import scodec.Attempt

      def exmap[A, B](
          fa: PartialCodec.Aux[W, A],
          f: A => Attempt[B],
          g: B => Attempt[A]
      ): PartialCodec.Aux[W, B] = new PartialCodec[B] {

        type WireType = fa.WireType

        protected[protobuf] def wireIncrement = fa.wireIncrement

        def force: Codec[B] = fa.force.exmap(f, g)

        def :=(index: Int)(implicit wireType: ValueOf[W]) = {
          import scodec.bits.BitVector
          import scodec.Attempt._
          import scodec.DecodeResult
          val x = (fa := index)
          new ProtobufCodec(x.prefixCodec, x.prefix, x.p.exmap(f, g)) {
            override def decode(bits: BitVector) = x.decode(bits) match {
              case Successful(DecodeResult(a, rem)) => f(a).map(DecodeResult(_, rem))
              case Failure(e)                       => Failure(e)
            }
            override def encode(b: B) =
              for {
                a <- g(b)
                res <- x.encode(a)
              } yield res
          }
        }
      }
    }

  /** Note this instance is here instead of `v3.implicits` package as we need to be able to override
    * by creating `PartialCodec` instances in the companions of case classes. If we put this in the
    * `v3.implicits` package then the blanket import overrides this. The spec is not explicit on the
    * search order of `S` and `T_n` in `S[T_1, ..., T_n]` but tests seem to indicate `T` is resolved
    * first.
    */
  implicit def implicitMessage[A, L <: HList, O <: HList](implicit
      isProduct: Generic.Aux[A, L],
      msg: Message.Aux[L, O],
      msgCodec: MessageCodec.Aux[O, L],
      xform: Transformer[L, A]
  ): PartialCodec.Aux[2, A] = {
    val _ = isProduct
    PartialCodec.LengthDelimited(msgCodec(msg(1)).as[A])
  }
}
