package scodec.protocols.protobuf.v3

import scodec._
import scodec.bits.BitVector
import scodec.Attempt._

import ProtobufCodec._

/** Codec which, unlike `scodec.codecs.DiscriminatorCodec`, distinguishes between mismatching
  * indices (out-of-order) and wire-types (error). It also allows inspecting the index with the
  * [[Protobuf.prefix]] member.
  */
class ProtobufCodec[A](
    val prefixCodec: Codec[Prefix],
    val prefix: Prefix,
    payload: => Codec[A]
) extends Codec[A] { self =>

  private[v3] lazy val p = payload

  def force: Codec[A] = p

  /** @throws [[ProtobufCodec.InvalidIndex]],[[ProtobufCodec.InvalidWireType]] */
  def decode(bits: BitVector): Attempt[DecodeResult[A]] =
    prefixCodec.decode(bits) flatMap {
      // TODO: this is highly inefficient for coproducts, O(n) comparison.
      case DecodeResult(Prefix(is, ws), rem) =>
        if (!(is sameElements prefix.index))
          Failure(InvalidIndex(prefix.index, is, Nil))
        else if (!(ws sameElements prefix.wireType))
          Failure(InvalidWireType(prefix.wireType, ws, Nil))
        else p.decode(rem)
    }

  /** Only writes the prefix if the value encodes to non-empty bits */
  def encode(value: A): Attempt[BitVector] = {
    for {
      pbits <- prefixCodec.encode(prefix)
      abits <- p.encode(value)
    } yield if (abits.isEmpty) BitVector.empty else pbits ++ abits
  }

  def sizeBound: SizeBound = prefixCodec.sizeBound + p.sizeBound
}

object ProtobufCodec {

  private val arrToStr = (is: Array[Int]) => is.map(_.toString).mkString("|")

  /** @param size
    *   The size of the element, including prefix. This can be used to skip the element, ''if'' it
    *   is of a fixed-size.
    */
  case class InvalidIndex(
      expected: Array[Int],
      actual: Array[Int],
      context: List[String]
  ) extends Err {
    def message: String =
      s"Expected index ${arrToStr(expected)}, decoded ${arrToStr(actual)}"
    def pushContext(ctx: String): Err = copy(context = ctx :: context)
  }

  case class InvalidWireType(
      expected: Array[Int],
      actual: Array[Int],
      context: List[String]
  ) extends Err {
    def message: String =
      s"Expected wire type ${arrToStr(expected)}, decoded ${arrToStr(actual)}"
    def pushContext(ctx: String): Err = copy(context = ctx :: context)
  }

  /** Constructor. Note that `payload` codec is lazily evaluated when decoding */
  def apply[A](prefixCodec: Codec[Prefix], prefix: Prefix, payload: => Codec[A]): ProtobufCodec[A] =
    new ProtobufCodec[A](prefixCodec, prefix, payload)

  /** When en/decoding, `encoded` must hold a single-element array. Otherwise this represents the
    * possible prefixes, in the case of a `oneOf` field. You can use a pattern match to extract the
    * decoded wire types and indices.
    */
  protected[protobuf] class Prefix(val encoded: Array[Int]) extends AnyVal {
    override def toString = Prefix.unapply(this) match {
      case Some((idx, tpe)) =>
        val t = tpe
          .map {
            case 0 => "{{u,s}int{32,64}, bool, enum}"
            case 1 => "{{s}fixed64, double}"
            case 2 => "{length-delimited}"
            case 5 => "{{s}fixed32, float}"
            case i => i.toString
          }
          .mkString("|")
        val i = idx.map(_.toString).mkString("|")
        s"Prefix($t := $i)"
      case None => throw new Error("Unknown type: $s")
    }

    def index: Array[Int] = encoded map (_ >>> 3)
    def wireType: Array[Int] = encoded map (_ & 0x0007)
  }

  protected[protobuf] object Prefix {
    def apply(index: Array[Int], wireType: Array[Int]): Prefix =
      new Prefix(index lazyZip wireType map (_ << 3 | _))
    def apply(index: Int, wireType: Int): Prefix =
      new Prefix(Array(index << 3 | wireType))
    def unapply(p: Prefix): Option[(Array[Int], Array[Int])] =
      Some((p.index, p.wireType))

    implicit def codec: Codec[Prefix] =
      scodec.codecs.vint
        .map(i => new Prefix(Array(i)))
        .econtramap[Prefix](
          _.encoded match {
            case Array(i) => Successful(i)
            case is       => Failure(Err(s"Unable to encode multiple prefixes: $is"))
          }
        )
        .fuse
  }

  implicit def invariantInstance: Transform[ProtobufCodec] =
    new Transform[ProtobufCodec] {
      def exmap[A, B](
          fa: ProtobufCodec[A],
          f: A => Attempt[B],
          g: B => Attempt[A]
      ): ProtobufCodec[B] = {
        import scodec.bits.BitVector
        import scodec.Attempt._
        import scodec.DecodeResult
        new ProtobufCodec(fa.prefixCodec, fa.prefix, fa.p.exmap(f, g)) {
          override def decode(bits: BitVector) = fa.decode(bits) match {
            case Successful(DecodeResult(a, rem)) => f(a).map(DecodeResult(_, rem))
            case Failure(e)                       => Failure(e)
          }
          override def encode(b: B) =
            for {
              a <- g(b)
              res <- fa.encode(a)
            } yield res
        }
      }
    }
}
