package scodec.protocols.protobuf.v3

/** This interface allows customizing the codecs used for primitives, and also supports using custom
  * types for unsigned integers. Simply mix-in your own implementation of [[Primitives]].
  */
trait ProtoBuf extends Primitives with Combinators {
  // implicit def defaultCharset: java.nio.charset.Charset // it doesn't like this
}
