package scodec.protocols.protobuf.v3
import scodec.bits._
import scodec.{Codec, Decoder, Attempt, DecodeResult, Err}
import scodec.codecs.{~, provide}
import scodec.Attempt._
import shapeless.{:: => _, _}
import ProtobufCodec.Prefix

protected trait MessageCodec[L <: HList] {
  type Out <: HList
  def apply(l: => L): ProtobufCodec[Out]
}

protected object MessageCodec {
  type Aux[L <: HList, M <: HList] = MessageCodec[L] { type Out = M }

  object Widen extends Poly1 {
    implicit def codec[A]: Case.Aux[ProtobufCodec[A], Codec[A]] =
      at { pc =>
        pc: Codec[A]
      }
  }

  /** @tparam L
    *   HList of Codec[*]
    * @tparam M
    *   HList of *
    */
  implicit def apply[L <: HList, M <: HList, N <: Nat, L2 <: HList](implicit
      codecsToArray: ops.hlist.ToArray[L, ProtobufCodec[_]],
      length: ops.hlist.Length.Aux[L, N],
      sizedToHList: ops.sized.ToHList[List[Any], N],
      widen: ops.hlist.Mapper.Aux[Widen.type, L, L2],
      traverseHListCodec: scodec.codecs.ToHListCodec.Aux[L2, M]
  ): MessageCodec.Aux[L, M] = new MessageCodec[L] {
    type Out = M

    locally { lazy val _ = length }

    def apply(l: => L): ProtobufCodec[Out] = {

      // TODO: DRY with [[MessageCodec]]
      val codecs: Array[ProtobufCodec[Any]] =
        codecsToArray(l)
          .asInstanceOf[Array[ProtobufCodec[Any]]] // ProtobufCodec is not covariant in A

      // TODO: consider using a BitSet instead of an Array[Int]
      val indices = codecs.map(_.prefix.index)

      val wireTypes = codecs.map(_.prefix.wireType)

      val maxIndex = if (indices.flatten.isEmpty) 0 else indices.flatten.max

      val p = Prefix(indices.flatten, wireTypes.flatten)

      // used by fn below
      val codecsByIndex =
        indices.zipWithIndex
          .foldLeft(Array.fill[Option[Int]](maxIndex + 1)(None)) { case (is, (i, j)) =>
            i.foldLeft(is)((is, i) => is.updated(i, Some(j)))
          }

      /* given wire index, return position of idx in `L`
       * @example {{{
       * // (int32 := 1, string := 3, int32 := 2)
       * outputIdx(3) == Some(1) //zero-based
       * }}}
       */
      def outputIdx(i: Int): Option[Int] =
        if (i < codecsByIndex.length) codecsByIndex(i) else None

      // used by fn below
      val codecsByPosition =
        codecs
          .foldLeft(Array.fill[Option[ProtobufCodec[Any]]](maxIndex + 1)(None)) { (cs, c) =>
            c.prefix.index.foldLeft(cs) { (cs, i) =>
              cs.updated(i, Some(c))
            }
          }

      /* codec to use at given index
       * @example {{{ //following on from above,
       * codecAt(3) == Some(string := 3)
       * }}}
       */
      def codecAt(i: Int): Option[ProtobufCodec[Any]] =
        if (i < codecsByPosition.length) codecsByPosition(i) else None

      // helper below
      def tuple[X, Y](l: Option[X], r: Option[Y]): Option[(X, Y)] =
        l.flatMap(l => r.map(l -> _))

      def listToTup(l: List[Any]): M =
        sizedToHList(Sized.wrap[List[Any], N](l))
          .asInstanceOf[M] // FIXME: strict typin

      def decoder: Decoder[M] = new Decoder[M] {
        def decode(bits: BitVector): Attempt[DecodeResult[M]] = {
          @annotation.tailrec
          def f(
              bits: BitVector
          )(
              as: Array[Any], // value - aligned with output
              zs: Array[Boolean] // is zero/empty/null - aligned with output
          ): Attempt[DecodeResult[(Array[Any], Array[Boolean])]] =
            if (bits.isEmpty) Successful(DecodeResult((as, zs), bits))
            else
              Prefix.codec.decode(bits) match {
                case Successful(DecodeResult(p @ Prefix(Array(i), Array(wireType)), payloadBits)) =>
                  tuple(codecAt(i), outputIdx(i)) match {
                    case Some((codec, j)) =>
                      if (!codec.prefix.wireType.exists(_ == wireType))
                        Failure(Err(s"Decode: expected wire type ϵ {${codec.prefix.wireType
                            .mkString("|")}} for index $i, but got $wireType"))
                      else
                        codec.decode(bits) match {
                          case Successful(DecodeResult(fa @ List(_), rem)) =>
                            if (zs(j))
                              f(rem)({ as(j) = fa; as }, { zs(j) = false; zs })
                            else
                              f(rem)(
                                {
                                  as(j) = as(j).asInstanceOf[List[Any]] ++ fa;
                                  as // TODO: append is O(n^2)
                                },
                                zs
                              )
                          case Successful(DecodeResult(fa @ Some(_), rem)) =>
                            if (zs(j))
                              f(rem)({ as(j) = fa; as }, { zs(j) = false; zs })
                            else
                              f(rem)({ as(j) = fa; as }, zs)
                          case Successful(DecodeResult(a, rem)) =>
                            f(rem)({ as(j) = a; as }, { zs(j) = false; zs })
                          case fail: Failure => fail
                        }
                    case None => // Unkown index - skip over it, for forward compat
                      // We ignore the result here; only care about the
                      // side-effect.  In fact we don't know the correct codec
                      // to use (only the wire type), so the value may be
                      // gibberish. All that matters is the length.

                      wireType match {
                        case 0 =>
                          f {
                            (int32: PartialCodec.Aux[0, Int]).force
                              .decode(payloadBits)
                              .require
                              .remainder
                          }(as, zs)
                        case 1 =>
                          f {
                            (sfixed64: PartialCodec.Aux[1, Long]).force
                              .decode(payloadBits)
                              .require
                              .remainder
                          }(as, zs)
                        case 2 =>
                          f { // fixed-length: decode the length field
                            int32.force
                              .decode(payloadBits)
                              .map { case DecodeResult(length, rem) =>
                                rem.drop(length * 8)
                              }
                              .require
                          }(as, zs)
                        case 3 =>
                          Failure(Err("Decoded deprecated wire type SGROUP - unsupported"))
                        case 4 =>
                          Failure(Err("Decoded deprecated wire type EGROUP - unsupported"))
                        case 5 =>
                          f {
                            (sfixed32: PartialCodec.Aux[5, Int]).force
                              .decode(payloadBits)
                              .require
                              .remainder
                          }(as, zs)
                        case x =>
                          Failure(Err(s"Decoded invalid wire type $x."))
                      }
                  }
                case Successful(DecodeResult(_, _)) =>
                  throw new Error("MessageCodec.decode.f: Impossible")
                case failedPrefix: Failure => failedPrefix
              }

          f(bits)(
            Array.ofDim[Any](indices.length),
            Array.fill(indices.length)(true)
          ).flatMap { case DecodeResult((as, zs), _) =>
            (zs zip as zip codecs)
              .foldRight[Attempt[DecodeResult[List[Any]]]](
                Successful(DecodeResult(Nil, BitVector.empty))
              ) {
                case (false ~ a ~ _, att) => att.map(_.map(a :: _))
                // If missing, try to decode an empty bitvector. If
                // it's a monoid, it will decode zero. Normal values
                // will throw an 'insufficient bits' error.
                case (true ~ _ ~ c, att) =>
                  att.flatMap { case DecodeResult(as, _) =>
                    c.decode(BitVector.empty).map(_.map(_ :: as))
                  }
                case ((_, _), _) => throw new Error("MessageCodec.decode: Impossible")
              }
          }.map(_.map(listToTup(_)))
        }
      }

      ProtobufCodec(
        provide(p),
        p,
        Codec(traverseHListCodec(widen(l)), decoder)
      )
    }
  }
}
