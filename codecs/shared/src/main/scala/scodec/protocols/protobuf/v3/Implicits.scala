package scodec.protocols.protobuf.v3

import scodec.Codec
import java.nio.charset.Charset
import shapeless.{HList, Generic, =:!=, ::, HNil, Typeable}

/** Mix-in to provide an 'implicit' package member which allows automatic derivation of codecs for
  * any product type (e.g. `case class`). A limitation of the current implementation is that fields
  * must be ordered and consecutive, as it's not possible to specify the index position. As a
  * result, when decoding, all fields must be present and ordered. Unspecified fields will raise a
  * decoding error.
  */
trait Implicits { self: Primitives =>

  val implicits: self.ImplicitPartialCodecs

  protected trait ImplicitPartialCodecs extends LowPrioImplicitCodecs {

    /** Wire type corresponding to `scala.Int`s. This will depend on the chosen primitive codec.
      */
    type IntWireType <: Int

    /** Wire type corresponding to `scala.Long`s. This will depend on the chosen primitive codec.
      */
    type LongWireType <: Int

    /** Wire type corresponding to [[Primitives#UInt]]s. This will depend on the chosen primitive
      * codec.
      */
    type UIntWireType <: Int

    /** Wire type corresponding to [[Primitives#ULong]]s. This will depend on the chosen primitive
      * codec.
      */
    type ULongWireType <: Int

    /** The default charset used by the [[string]] codec. This must be qualified as `implicit` when
      * overridding.
      */
    def defaultCharset: Charset

    implicit def implicitInt: PartialCodec.Aux[IntWireType, Int]

    implicit def implicitLong: PartialCodec.Aux[LongWireType, Long]

    implicit def implicitUInt: PartialCodec.Aux[UIntWireType, UInt]

    implicit def implicitULong: PartialCodec.Aux[ULongWireType, ULong]

    implicit def implicitDouble: PartialCodec.Aux[1, Double]

    implicit def implicitFloat: PartialCodec.Aux[5, Float]

    implicit def implicitBool: PartialCodec.Aux[0, Boolean]

    implicit def implicitString(implicit
        cs: Charset
    ): PartialCodec.Aux[2, String]

    implicit def implicitBytes: PartialCodec.Aux[2, scodec.bits.ByteVector]

    implicit def implicitMap[K, V, W, X](implicit
        kCodec: PartialCodec.Aux[W, K],
        vCodec: PartialCodec.Aux[X, V],
        W: ValueOf[W],
        X: ValueOf[X],
        K: Typeable[K],
        V: Typeable[V]
    ): PartialCodec.Aux[2, Map[K, V]] =
      mapField(kCodec, vCodec)

    /** By default, primitive types resolve to the packed list codec. */
    implicit def implicitPackedList[A, W <: Int](implicit
        field: PartialCodec.Aux[W, A],
        isScalar: W =:!= 2
    ): PartialCodec.Aux[2, List[A]] =
      repeated(field, packed = true)

    implicit def implicitOptional[A, W <: Int](implicit
        field: PartialCodec.Aux[W, A]
    ): PartialCodec.Aux[W, Option[A]] =
      optional(field)

    implicit def implicitEnum[E <: Enumeration](implicit
        E: ValueOf[E]
    ): PartialCodec.Aux[0, E#Value] =
      enum[E].asInstanceOf[PartialCodec.Aux[0, E#Value]]

    /* TODO: should the `ValueOf` implicits be somewhere else? Currently requires
     * importing `v3.implicits._` to be used. Maybe in [[v3]] instead?
     * Also, head is context bound on Int because `Lazy` doesn't appear to
     * work with `ValueOf`
     */
    implicit def valueOfHListCons[H <: Int, T <: HList](implicit
        H: ValueOf[H],
        T: ValueOf[T]
    ): ValueOf[H :: T] =
      new ValueOf(H.value :: T.value)

    implicit lazy val valueOfHNil: ValueOf[HNil] = new ValueOf(HNil)

    /** This is the entry point. Note it returns a `Codec[A]`, not a [[PartialCodec]] like the other
      * implicits. This means it does not prefix the values with an index or wire-type.
      *
      * @tparam A
      *   a `case class`-like type, as evidenced by `isProduct` param.
      */
    implicit def autoderive[A](implicit
        isProduct: Generic.Aux[A, _ <: HList],
        pc: PartialCodec.Aux[2, A]
    ): Codec[A] = {
      val _ = isProduct
      pc.force
    }

    /* `implicit def implicitMessage` is in companion of [[PartialCodec]] */
  }
}

protected trait LowPrioImplicitCodecs {

  /** Packed codecs take priority over this */
  implicit def implicitUnpackedList[A, W <: Int](implicit
      field: PartialCodec.Aux[W, A]
  ): PartialCodec.Aux[W, List[A]] =
    repeated(field, packed = false)
}
