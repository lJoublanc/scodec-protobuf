package scodec.protocols.protobuf.v3
import scodec.bits._
import scodec.{Codec, Attempt, DecodeResult, SizeBound, Err}
import scodec.codecs.provide
import shapeless.{:: => _, _}
import ProtobufCodec.{Prefix, InvalidIndex}

/** Note this doesn't work for recursive OneOf's - not sure what the protocol has to say about that.
  */
trait OneOfCodec[L <: HList] {
  type Out <: Coproduct
  def apply(l: L): ProtobufCodec[Out]
}

object OneOfCodec extends {

  type Aux[L <: HList, C <: Coproduct] = OneOfCodec[L] { type Out = C }

  // TODO: Add a unit test for this
  //format: off
  /** Consider using `Combinators.oneOf(fields : _*)` instead. This constructor
   *  is mainly useful for cases with > 22 fields.
   *
   *  @param coprod A shapeless `HList` of `ProtobufCodec`.
   *  @example {{{
   *  OneOfCodec(
   *      { string := 1 }
   *   :: { int32 := 29 }
   *   :: { ValName.partialCodec := 2 }
   *   :: { PartialCodec[BuiltinFunction] := 3 }
   *   :: { PartialCodec[PrimCon] := 4 }
   *   :: { PartialCodec[PrimLit] := 5 }
   *   :: { PartialCodec[RecCon] := 6 }
   *   :: { PartialCodec[RecProj] := 7 }
   *   ...
   *   :: { PartialCodec[RecUpd] := 22
   *   :: HNil
   *  )
   *  }}}
   */
  def apply[L <: HList](fields : L)(
      implicit tc: OneOfCodec[L]
  ): ProtobufCodec[tc.Out] = tc.apply(fields)
  //format: on

  implicit def apply[L <: HList, M <: HList, C <: Coproduct](implicit
      codecsToArray: ops.hlist.ToArray[L, ProtobufCodec[_]],
      comapped: ops.hlist.Comapped.Aux[L, ProtobufCodec, M],
      toCoprod: ops.hlist.ToCoproduct.Aux[M, C]
  ): OneOfCodec.Aux[L, C] = new OneOfCodec[L] {
    type Out = C

    locally { val _ = (comapped, toCoprod) }

    def apply(l: L): ProtobufCodec[Out] = {
      // TODO: DRY with [[MessageCodec]]
      val codecs: Array[ProtobufCodec[Any]] =
        codecsToArray(l)
          .asInstanceOf[Array[ProtobufCodec[Any]]] // ProtobufCodec is not covariant in A

      // TODO: consider using a BitSet instead of an Array[Int]
      val indices = codecs.map(_.prefix.index)

      val wireTypes = codecs.map(_.prefix.wireType)

      val maxIndex = if (indices.flatten.isEmpty) 0 else indices.flatten.max

      val p = Prefix(indices.flatten, wireTypes.flatten)

      /* given wire index, return position of idx in `L`
       * @example {{{
       * // (int32 := 1, string := 3, int32 := 2)
       * outputIdx(3) == Some(1) //zero-based
       * }}}
       */
      val outputIdx: Int => Option[Int] =
        indices.zipWithIndex
          .foldLeft(Array.fill[Option[Int]](maxIndex + 1)(None)) { case (is, (i, j)) =>
            i.foldLeft(is)((is, i) => is.updated(i, Some(j)))
          }
          .apply(_)

      // TODO: replace these two helpers with shapless.coproduct.ops.inject etc
      // TODO: make @annotation.tailrec
      /** @return A coproduct with value `a`, of type at position `i` */
      def nest(i: Int)(a: Any): Coproduct =
        if (i <= 0) Inl(a) else Inr(nest(i - 1)(a))

      /** @return index in the coproduct, and the untyped value */
      def extract(c: Coproduct, i: Int = 0): (Int, Any) = c match {
        case Inl(a) => (i, a)
        case Inr(c) => extract(c, i + 1)
      }

      val c = new Codec[C] {

        /* Attempt to decode with the first codec, recover by using the decoded index.
         * This is preferrable to decoding the prefix, as we lose error context.
         */
        def decode(bits: BitVector): Attempt[DecodeResult[C]] =
          if (bits.isEmpty) Attempt.Failure {
            Err.InsufficientBits(
              Prefix.codec.sizeBound.lowerBound,
              0L,
              "oneOf" :: Nil
            )
          }
          else
            codecs.head
              .decode(bits)
              .map(_.map(nest(0)(_).asInstanceOf[C]))
              .recoverWith { case e @ InvalidIndex(_, Array(i), ctx) =>
                Attempt
                  .fromOption(
                    for {
                      _ <- Option.unless(i > maxIndex)(i)
                      j <- outputIdx(i)
                      c = codecs(j)
                    } yield c.decode(bits).map(_.map(nest(j)(_).asInstanceOf[C])),
                    e.copy(expected = indices.flatten)
                  )
                  .flatten
              }
              .mapErr(_.pushContext("oneOf"))

        def encode(c: C): Attempt[BitVector] = {
          val (i, a) = extract(c)
          codecs(i).encode(a).mapErr(_.pushContext("oneOf"))
        }

        def sizeBound = SizeBound.choice(codecs.map(_.sizeBound))
      }

      ProtobufCodec(provide(p), p, c)
    }
  }
}
