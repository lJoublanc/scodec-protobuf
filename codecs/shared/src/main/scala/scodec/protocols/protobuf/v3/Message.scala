package scodec.protocols.protobuf.v3

import shapeless._

/** Turns an `HList` `L` into a list of protobuf `Codec`s, indexed by position, starting at
  * `startIndex`
  */
trait Message[L <: HList] {

  /** `HList` of `Codec[_]` */
  type Out <: HList

  def apply(startIndex: Int = 1): Out
}

object Message {

  def apply[L <: HList](implicit
      tc: Message[L]
  ): Message.Aux[L, tc.Out] = tc

  type Aux[L <: HList, O <: HList] = Message[L] { type Out = O }

  implicit val nil: Message.Aux[HNil, HNil] =
    new Message[HNil] {

      type Out = HNil

      def apply(lastIndex: Int) = HNil
    }

  implicit def cons[H, W, T <: HList](implicit
      pc: PartialCodec.Aux[W, H],
      wireType: ValueOf[W],
      tail: Message[T]
  ): Message.Aux[H :: T, ProtobufCodec[H] :: tail.Out] =
    new Message[H :: T] {

      private lazy val p = pc

      type Out = ProtobufCodec[H] :: tail.Out

      def apply(startIndex: Int) =
        p.:=(startIndex) :: tail(startIndex + p.wireIncrement)
    }
}
