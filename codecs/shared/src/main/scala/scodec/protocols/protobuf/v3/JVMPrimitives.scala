package scodec.protocols.protobuf.v3

import scodec.{TransformSyntax, Transform, Attempt, Err}
import scodec.codecs

import java.nio.charset.StandardCharsets.{UTF_8, US_ASCII}

/** Uses the default Scala/JVM types as primitives. Particularly, uses signed integer AnyVal to
  * represent unsigned types. This means that codecs may `throw` at runtime if values outside range.
  */
trait JVMPrimitives extends Primitives {

  type UInt = JVMPrimitives.UInt

  type ULong = JVMPrimitives.ULong

  import JVMPrimitives._

  def uintIsIntegral: Integral[UInt] = UInt.integralInstance

  def ulongIsIntegral: Integral[ULong] = ULong.integralInstance

  private def toUnsigned[I, U, W <: Int](
      I: PartialCodec.Aux[W, I],
      f: I => Option[U],
      g: U => I
  ): PartialCodec.Aux[W, U] =
    I.narrow[U](
      i => Attempt.fromOption(f(i), Err(s"decode unsigned outside range: $i")),
      g
    )

  def double = PartialCodec[1, Double](codecs.double)

  def float = PartialCodec[5, Float](codecs.float)

  def int32 = PartialCodec[0, Int](codecs.vint)

  def int64 = PartialCodec[0, Long](codecs.vlong)

  def uint32 = toUnsigned(int32, UInt.safe, _.toInt)

  def uint64 = toUnsigned(int64, ULong.safe, _.toLong)

  def fixed32 = toUnsigned(sfixed32, UInt.safe, _.toInt)

  def fixed64 = toUnsigned(sfixed64, ULong.safe, _.toLong)

  def sfixed32 = PartialCodec[5, Int](codecs.int32L)

  def sfixed64 = PartialCodec[1, Long](codecs.int64L)

  def string(implicit charset: java.nio.charset.Charset) =
    PartialCodec[2, String] {
      lengthDelimited(codecs.string(charset))
    }.ensuring(charset == UTF_8 || charset == US_ASCII)

  def bytes =
    PartialCodec[2, scodec.bits.ByteVector](lengthDelimited(codecs.bytes))

  protected[protobuf] def lengthDelimited[A](c: scodec.Codec[A]) =
    scodec.codecs.variableSizeBytes(codecs.vint, c, 0)
}

object JVMPrimitives {

  /** Unsigned integer, with range 0 - 2^31. */
  case class UInt(toInt: Int) extends AnyVal

  object UInt {

    /** Safe constructor, ensures you can't create negative `UInt`. */
    def safe(fromInt: Int): Option[UInt] =
      Option.unless(fromInt < 0)(UInt(fromInt))

    implicit def integralInstance: Integral[UInt] =
      Integral[Int]
        .narrowc[UInt](i => Attempt.fromOption(UInt.safe(i), Err(s"Negative UInt: $i")))(_.toInt)
  }

  /** Unsigned integer, with range 0 - 2^63. */
  case class ULong(toLong: Long) extends AnyVal

  object ULong {

    /** Safe constructor, ensures you can't create negative `ULong`. */
    def safe(fromLong: Long): Option[ULong] =
      Option.unless(fromLong < 0)(ULong(fromLong))

    implicit def integralInstance: Integral[ULong] =
      Integral[Long]
        .narrowc(i => Attempt.fromOption(ULong.safe(i), Err(s"Negative ULong: $i")))(_.toLong)
  }

  implicit def invariantIntegral: Transform[Integral] =
    new Transform[Integral] {
      def exmap[A, B](fa: Integral[A], f: A => Attempt[B], g: B => Attempt[A]) =
        new Integral[B] {

          private def bin(h: (A, A) => A, x: B, y: B): B = {
            for {
              x <- g(x)
              y <- g(y)
              f <- f(fa.quot(x, y))
            } yield f
          }.require

          private def unary[C](h: A => C, x: B): C =
            g(x).map(h).require

          def quot(x: B, y: B): B = bin(fa.quot, x, y)
          def rem(x: B, y: B): B = bin(fa.rem, x, y)

          def fromInt(x: Int): B = f(fa.fromInt(x)).require
          def minus(x: B, y: B): B = bin(fa.minus, x, y)
          def negate(x: B): B = f(unary(fa.negate, x)).require
          def parseString(str: String): Option[B] =
            fa.parseString(str).flatMap(f(_).toOption)
          def plus(x: B, y: B): B = bin(fa.plus, x, y)
          def times(x: B, y: B): B = bin(fa.times, x, y)
          def toDouble(x: B): Double = unary(fa.toDouble, x)
          def toFloat(x: B): Float = unary(fa.toFloat, x)
          def toInt(x: B): Int = unary(fa.toInt, x)
          def toLong(x: B): Long = unary(fa.toLong, x)

          def compare(x: B, y: B): Int = {
            for {
              x <- g(x)
              y <- g(y)
            } yield fa.compare(x, y)
          }.require

        }
    }
}
