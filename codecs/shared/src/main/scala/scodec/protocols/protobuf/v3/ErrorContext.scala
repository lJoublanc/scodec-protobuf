package scodec.protocols.protobuf.v3

import scodec.Codec

/** Mix-in that adds error context messages to all the primitive messages */
protected trait ErrorContext extends ProtoBuf {

  /* Primitives */

  abstract override def bool = super.bool withContext "double"

  abstract override def bytes = super.bytes withContext "bytes"

  abstract override def double = super.double withContext "double"

  abstract override def fixed32 = super.fixed32 withContext "fixed32"

  abstract override def fixed64 = super.fixed64 withContext "fixed64"

  abstract override def float = super.float withContext "float"

  abstract override def int32 = super.int32 withContext "int32"

  abstract override def int64 = super.int64 withContext "int64"

  abstract override def sfixed32 = super.sfixed32 withContext "sfixed32"

  abstract override def sfixed64 = super.sfixed64 withContext "sfixed64"

  // abstract override def sint32 = super.sint32 withContext "sint32"
  //
  // abstract override def sint64 = super.sint64 withContext "sint64"
  //
  abstract override def string(implicit charset: java.nio.charset.Charset) =
    super.string(charset).withContext("string")

  abstract override def uint32 = super.uint32 withContext "uint32"

  abstract override def uint64 = super.uint64 withContext "uint64"

  /* Combinators */

  abstract override def optional[W <: Int, A](field: => PartialCodec.Aux[W, A]) =
    super.optional(field) withContext "optional"

  /* These are killing the compiler: java.lang.ClassFormatError: Duplicate method name "repeated"
   *
  abstract override def repeated[A, W <: Int](
      field: PartialCodec.Aux[W, A],
      packed: true = true
  )(
      implicit isScalar: shapeless.=:!=[W, 2]
  ) = super.repeated(field, packed = true) withContext "packed"

  abstract override def repeated[A, W <: Int](
      field: PartialCodec.Aux[W, A],
      packed: false
  ) = super.repeated(field, packed = false) withContext "unpacked"
   */

  // Can't override this as the return type is ProtoBufCodec, but withContext is final
  /*
  abstract override def oneOf[A, B](first: ProtobufCodec[A], second: ProtobufCodec[B]) =
    super.oneOf(first, second) withContext "oneOf"
   */

  abstract override def `enum`[E <: Enumeration](implicit E: ValueOf[E]) =
    super.enum withContext "enum"

  import shapeless._

  abstract override def message[A](codec: => ProtobufCodec[A])(implicit
      msgCodec: MessageCodec.Aux[shapeless.::[ProtobufCodec[A], HNil], shapeless.::[A, HNil]]
  ): PartialCodec.Aux[2, A] =
    super.message(codec) withContext "message"

  abstract override def message[P, L <: HList, M <: HList, A](tuple: => P)(implicit
      gen: Generic.Aux[P, L],
      msgCodec: MessageCodec.Aux[L, M],
      tupler: ops.hlist.Tupler.Aux[M, A],
      xform: scodec.Transformer[M, A]
  ) = super.message(tuple) withContext "message"
}
