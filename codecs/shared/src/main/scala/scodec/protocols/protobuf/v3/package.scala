package scodec.protocols.protobuf

import java.nio.charset.Charset

/** =Google Protocol Buffers=
  *
  * Default implementation of protocol-buffers, with unsigned types represented by signed JVM `Int`
  * and `Long` value types, and automatic derivation for case classes using varints when
  * ''encoding''.
  *
  * This is non-conformant in the following ways:
  *   - Fields are not considered optional by default
  *
  * If you want to customize codecs, see [[v3.ProtoBuf]].
  *
  * ==Explicit codecs==
  *
  * {{{
  * scala> import scodec.protocols.protobuf.v3._
  *        import scodec.TransformSyntax //for _.as syntax
  *
  * scala> case class Foo(bar: Int, baz: String)
  * defined class Foo
  *
  * scala> message(int32 := 1, string := 2).as[Foo]
  * res1: scodec.ProtobufCodec[Foo]
  *
  * scala> res1.encode(Foo(1, "two"))
  * res2: scodec.Attempt[scodec.bits.BitVector] = Successful(BitVector(56 bits, 0x0801120374776f))
  * }}}
  * Note you should not use `scodec`'s combinators such as `::`, as these imply **in-order decoding
  * **. Instead use [[v3.message]] and [[v3.oneOf]] to allow out-of-order decoding.
  *
  * ==Implicit codecs==
  *
  * {{{
  * scala> import scodec.protocols.protobuf.v3.implicits._
  *
  * scala> scodec.Codec[Foo].decode(res2.require)
  * res4: scodec.Attempt[scodec.DecodeResult[Foo]] = Successful(DecodeResult(Foo(1,two),BitVector(empty)))
  *
  * }}}
  *
  * Note that implicit codecs will only resolve for top-level message objects i.e. `case class`es.
  * For example, trying to summon a `Codec[Int]` will fail, but `Codec[Tuple1[Int]]` will work.
  *
  * So, if you have automatic implicit derivation, why would you want to use explicit codecs at all?
  *   - You are replicating a schema with out-of-order field indices.
  *   - Your schema has sum types / coproducts. Coproduct codecs are WIP.
  */
package object v3 extends ProtoBuf with JVMPrimitives with ErrorContext with JVMImplicits {

  implicit lazy val defaultCharset: Charset = implicits.defaultCharset
}
