package scodec.protocols.protobuf.v3

import scodec.Codec
import scodec.Attempt.{successful, failure}

/** Codecs for the primitive elements in protobuffers. These are not 'full' codecs as they don't
  * include framing information, i.e. the index + wire-type information.
  *
  * The API doc has been mostly copied from
  * [[https://developers.google.com/protocol-buffers/docs/proto3#scalar this]] table.
  *
  * @see
  *   [[https://developers.google.com/protocol-buffers/docs/encoding]] for the specification.
  */
trait Primitives { self =>

  /** The type used to represent unsinged 32-bit integers. */
  type UInt

  /** The type used to represent unsinged 64-bit integers. */
  type ULong

  /** Witness that [[UInt]] is a `scala.math.Integral`. This guarantees portability across
    * platforms. A typical implementation will have an `implicit instance: Integral[UInt]` in the
    * companion of `UInt`.
    * @example
    *   {{{ scala> import JVMPrimitives.{UInt, ULong} scala> import Integral.Implicits._
    *
    * scala> UInt(1) + UInt(2) res1: scodec.protocols.protobuf.v3.JVMPrimitives.UInt = UInt(3) }}}
    */
  def uintIsIntegral: Integral[UInt]

  /** Witness that [[ULong]] is a `scala.math.Integral`.
    * @see
    *   [[uintIsIntegral]] for further explanation
    */
  def ulongIsIntegral: Integral[ULong]

  /** IEEE 754 double precision binary. */
  def double: PartialCodec.Aux[1, Double]

  /** IEEE 754 single precision binary. */
  def float: PartialCodec.Aux[5, Float]

  /** Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is
    * likely to have negative values, use [[sint32]] instead.
    */
  def int32: PartialCodec.Aux[0, Int]

  /** Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is
    * likely to have negative values, use [[sint64]] instead.
    */
  def int64: PartialCodec.Aux[0, Long]

  /** Uses variable-length encoding. */
  def uint32: PartialCodec.Aux[0, UInt]

  /** Uses variable-length encoding. */
  def uint64: PartialCodec.Aux[0, ULong]

  /** Uses variable-length encoding. Signed `int` value. These more efficiently encode negative
    * numbers than regular [[int32]]s.
    */
  def sint32: PartialCodec.Aux[0, Int] =
    int32.xmap[Int](n => (n >> 1) ^ (-(n & 1)), n => (n << 1) ^ (n >> 31))

  /** Uses variable-length encoding. Signed `int` value. These more efficiently encode negative
    * numbers than regular [[int64]]s.
    */
  def sint64: PartialCodec.Aux[0, Long] =
    int64.xmap[Long](n => (n >> 1) ^ (-(n & 1)), n => (n << 1) ^ (n >> 63))

  /** Always four bytes. More efficient than [[uint32]] if values are often greater than 228.
    */
  def fixed32: PartialCodec.Aux[5, UInt]

  /** Always eight bytes. More efficient than [[uint64]] if values are often greater than 256.
    */
  def fixed64: PartialCodec.Aux[1, ULong]

  /** Always four bytes. */
  def sfixed32: PartialCodec.Aux[5, Int]

  /** Always eight bytes. */
  def sfixed64: PartialCodec.Aux[1, Long]

  /** Always one byte. */
  def bool: PartialCodec.Aux[0, Boolean] =
    int32.narrowc {
      case 0 => successful(false)
      case 1 => successful(true)
      case _ => failure(scodec.Err("decode bool outside 0|1 range"))
    } {
      case true  => 1
      case false => 0
    }

  /** A string must always contain UTF-8 encoded or 7-bit ASCII text, and cannot be longer than 232.
    * @param charset
    *   Default to UTF-8.
    */
  def string(implicit
      charset: java.nio.charset.Charset
  ): PartialCodec.Aux[2, String]

  /** May contain any arbitrary sequence of bytes no longer than 232. */
  def bytes: PartialCodec.Aux[2, scodec.bits.ByteVector]

  protected[protobuf] def lengthDelimited[A](c: Codec[A]): Codec[A]
}
