import grpc._
import java.net.URI
import cats.effect._
import scodec.protocols.protobuf.v3.implicits._
import fs2.Stream

/** This is Google API 'library agent' example service that supports gRPC. The `*.proto` definitions
  * are [[https://github.com/googleapis/googleapis/tree/master/google/example/library on github]]
  * And you can play around with it interactively
  * [[https://developers.google.com/apis-explorer/#search/example/libraryagent/v1/ here]]. for
  * example adding some books so that the 'list shelves' call below actually returns something.
  *
  * @todo
  *   Currently all this does is return a 403 Forbidden, which means it's working. Need to implement
  *   Oauth2.
  */
object GoogleLibraryAgent extends IOApp {

  case class Shelf(
      name: String,
      theme: String
  )

  case class ListShelvesRequest(
      page_size: Int,
      page_token: String
  )

  case class ListShelvesResponse(
      shelves: List[Shelf],
      next_page_token: String
  )

  case class LibraryService[F[_]](
      shelves: ListShelvesRequest => F[ListShelvesResponse]
  )

  val uri = new URI("https://libraryagent.googleapis.com/v1/")

  def run(args: List[String]): IO[ExitCode] = {
    for {
      svc <- Service[IO](new URI("https://www.googleapis.com/books/v1"))
      library <- svc.of[LibraryService]()
      req = ListShelvesRequest(10, "tok")
      rep <- Stream eval library.shelves(req)
      _ <- Stream eval (IO { println(s"Response: $rep") })
    } yield ExitCode.Success
  }.compile.lastOrError
}
