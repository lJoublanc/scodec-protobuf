package grpc

import shapeless.{HList, Poly0, LabelledGeneric, Nat, Poly2}
import shapeless.ops.hlist.{FillWith, Mapped, Length, Fill, ZipWith}
import fs2.Stream

import java.net.URI
import cats.effect.Resource

/** A protobuf service builder. Uses meta programming techniques to generate service RPC definitions
  * based off the requested type signature. The service RPC's are represented by function literals
  * inside a case class, of type:
  *
  *   1. `I => F[O]` unary request. 2. `I => Stream[F, O]` server-streaming request. 3. `Stream[F,
  *      I] => F[O]` client-streaming request. 4. `Stream[F, I] => Stream[F, O]` bidirectional
  *      streaming.
  *
  * Which correcpond to the
  * [[https://www.grpc.io/docs/guides/concepts/#rpc-life-cycle respective gRPC calls]].
  *
  * @example
  *   {{{}}}
  */
trait Service[F[_]] { self =>

  type Rpc <: Poly0

  def baseUri: URI

  /** Create an instance of a protobuf service, tied to the specified `baseUri`.
    *
    * @tparam G
    *   Typically a `scala.Product` corresponding of the rpc calls of the protobuf service
    */
  def of[G[_[_]]] = new OfSyntax[G] {}

  trait OfSyntax[G[_[_]]] {
    /* This is pretty convoluted. It turns the case class into a list of I =>
     * O, then prepends => URI to them, derives the URI from HasFqN, and
     * partial applies it n.b. `Rpc` has methods of the form URI => I => O.
     */
    def apply[L <: HList, M <: HList, C <: HList, N <: Nat]()( // TODO: get rid off the pesky `()`
        implicit
        svcName: HasFqn[G[F]],
        labels: LabelledGeneric.Aux[G[F], L],
        mapped: Mapped.Aux[L, ({ type λ[a] = URI => a })#λ, M],
        mkRpc: FillWith[self.Rpc, M],
        length: Length.Aux[L, N],
        fqns: Fill.Aux[N, URI, C],
        zipWith: ZipWith.Aux[M, C, Service.ApplyUrl.type, L]
    ): Stream[F, G[F]] = {
      val uris = fqns(new URI(svcName.fqn))
      val _ = mapped // supress warnings
      val _ = length
      Stream[F, G[F]](labels.from(zipWith(mkRpc(), uris)))
    }
  }

}

object Service {
  import cats.effect.Async
  import okhttp3.{OkHttpClient, Protocol}
  import scala.jdk.CollectionConverters.MutableSeqHasAsJava

  // FIXME should return a `Client` rather than `Http4sClient` but that breaks.
  /** Creates a service builder backed by http4s + OkHttp client.
    * @baseUri
    *   Base URL of the service to connect to
    * @certificates
    *   Paths to additional PEM certificates
    */
  def apply[F[_]: Async](
      baseUri: URI,
      certificates: fs2.io.file.Path*
  ): Stream[F, Http4sClientService[F]] = {
    import java.security.cert.X509Certificate
    import okhttp3.tls.{Certificates, HandshakeCertificates}
    import fs2.io.file
    import fs2.text

    def loadCertificate(path: file.Path): Stream[F, X509Certificate] =
      file
        .Files[F]
        .readAll(path)
        .through(text.utf8.decode)
        .map(Certificates.decodeCertificatePem(_))

    for {
      blocker <- Stream resource Resource.unit[F] // TODO : remove this; it's a no-op
      cs <- Stream emits certificates.toList flatMap loadCertificate foldMap (cert => List(cert))
      builder = org.http4s.okhttp.client.OkHttpBuilder[F](
        baseUri.getScheme() match {
          case "http" =>
            new OkHttpClient.Builder()
              .protocols(scala.collection.mutable.Seq(Protocol.H2_PRIOR_KNOWLEDGE).asJava)
              // Note this is distinct from GRPC timeout (in headers)
              .readTimeout(java.time.Duration.ZERO)
              .build()
          case "https" =>
            val certs =
              cs.foldLeft(new HandshakeCertificates.Builder())(_ addTrustedCertificate _).build()
            new OkHttpClient.Builder()
              .protocols(scala.collection.mutable.Seq(Protocol.HTTP_2, Protocol.HTTP_1_1).asJava)
              .sslSocketFactory(certs.sslSocketFactory(), certs.trustManager())
              // Note this is distinct from GRPC timeout (in headers)
              .readTimeout(java.time.Duration.ZERO)
              .build()
          case s =>
            throw new Error(s"Service: unsupported scheme: $s")
        }
      )
      cli <- builder.stream
    } yield new Http4sClientService(cli, baseUri)
  }

  /** Partially applies URI to fn of the form URI => I => O
    * @return
    *   `I => O`
    */
  protected object ApplyUrl extends Poly2 {
    implicit def g[A]: Case.Aux[URI => A, java.net.URI, A] =
      at { (g, uri) =>
        g(uri)
      }
  }
}
