package grpc

/** Derive the fully qualified name of a type `A`. This is used to infer the path of the GRPC
  * service from the packge name.
  * @param fqn
  *   The fully qualified name i.e. class name prefixed with pkg
  * @param pkg
  *   The package name/prefix.
  * @param name
  *   The name, not including package prefix.
  */
trait HasFqn[A] {
  def fqn: String
  def pkg: String
  def name: String
}

object HasFqn {
  import scala.reflect.ClassTag
  implicit def any[A](implicit ct: ClassTag[A]): HasFqn[A] = new HasFqn[A] {
    def fqn: String = ct.runtimeClass.getName
    def pkg: String = fqn.split('.').init.mkString(".")
    def name: String = fqn.split('.').last
  }

  def apply[A](implicit A: HasFqn[A]) = A
}
