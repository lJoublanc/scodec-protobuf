package grpc

import scodec.Codec
import shapeless._
import shapeless.poly.Case0
import shapeless.labelled.{FieldType, field}
import fs2.Stream
import fs2.interop.scodec.StreamDecoder
import cats.effect.Concurrent
import cats.data.NonEmptyList
import org.http4s.client.Client
import org.http4s.headers.`Content-Type`
import org.http4s.ContentCoding
import org.typelevel.ci.CIString

import scala.language.postfixOps
import java.net.URI

class Http4sClientService[F[_]: Concurrent](
    val channel: Client[F],
    val baseUri: URI,
    val encoding: ContentCoding = ContentCoding.identity
) extends Service[F] {

  type Rpc = Rpc.type

  object Rpc extends Poly0 {
    import shapeless._
    import org.http4s._
    import cats.instances.function._
    import cats.syntax.profunctor._

    implicit def nil: Case0.Aux[Rpc.type, HNil] = at { HNil }

    implicit def unary[K <: Symbol, I: Codec, O: Codec](implicit
        label: Witness.Aux[K]
    ): Case0.Aux[Rpc.type, URI => FieldType[K, I => F[O]]] = at {
      bidirectionalStream[K, I, O]
        .apply()
        .rmap { f =>
          field[K] {
            f.dimap(Stream.emit[F, I])(_.head.compile.lastOrError)
          }
        }
    }

    implicit def serverStream[K <: Symbol, I: Codec, O: Codec](implicit
        label: Witness.Aux[K]
    ): Case0.Aux[Rpc.type, URI => FieldType[K, I => Stream[F, O]]] = at {
      bidirectionalStream[K, I, O]
        .apply()
        .rmap { f =>
          field[K] {
            f.lmap(Stream.emit[F, I])
          }
        }
    }

    implicit def clientStream[K <: Symbol, I: Codec, O: Codec](implicit
        label: Witness.Aux[K]
    ): Case0.Aux[Rpc.type, URI => FieldType[K, Stream[F, I] => F[O]]] = at {
      bidirectionalStream[K, I, O]
        .apply()
        .rmap { f =>
          field[K] {
            f.rmap(_.head.compile.lastOrError)
          }
        }
    }

    implicit def bidirectionalStream[K <: Symbol, I: Codec, O: Codec](implicit
        label: Witness.Aux[K]
    ): Case0.Aux[Rpc.type, URI => FieldType[K, Stream[F, I] => Stream[F, O]]] =
      at { svcUri =>
        field[K] { (is: Stream[F, I]) =>
          val S = Stream.monadErrorInstance[F]

          for {
            uri <- S.fromEither(
              Uri.fromString(
                baseUri resolve List(
                  baseUri.getPath,
                  svcUri,
                  label.value.name.capitalize
                ) // TODO: Not sure if this convention of capitalizing will always work (what if we define an rpc call with a lower-case name?)
                  .mkString("/") toString
              )
            )
            appGrpc <- S.fromEither(MediaType.parse("application/grpc+proto"))
            i <- is
            bitsOut <- S.fromTry(
              lengthPrefixedMessage(encoding, Codec[I]).encode(i).toTry
            )
            bytesOut = bitsOut.bytes
            req = Request(
              method = Method.POST,
              uri = uri,
              httpVersion = HttpVersion.`HTTP/2`,
              headers = Headers(
                // See https://github.com/grpc/grpc/blob/master/doc/PROTOCOL-HTTP2.md
                Header.Raw(CIString("TE"), "trailers"),
                // Header("grpc-timeout", "24H") //TODO: should probably match that passed to the Okhttpclient
                Header.Raw(CIString("grpc-encoding"), encoding.renderString),
                Header.Raw(CIString("grpc-accept-encoding"), encoding.renderString),
                Header.Raw(CIString("content-length"), bytesOut.length.toString)
              ),
              body = Stream.emits(bytesOut.toSeq).covary[F]
            ).withContentType(`Content-Type`(appGrpc))
            rep <- channel.stream(req)
            _ <- assertHeaderErrors(rep)
            repCoding <- S.fromEither(getResponseEncoding(rep))
            o <- rep.body.through {
              StreamDecoder.many(lengthPrefixedMessage(encoding, Codec[O])).toPipeByte
            }
          } yield o
        }
      }

    /** Parses the headers to determine the content coding. returns `identity` if header is missing.
      */
    protected def getResponseEncoding(
        response: Response[F]
    ): Either[Throwable, ContentCoding] =
      response.headers
        .get(CIString("grpc-encoding"))
        .fold[Either[Throwable, ContentCoding]](Right(ContentCoding.identity)) {
          case NonEmptyList(hdr, Nil) => ContentCoding parse hdr.value
          case _ =>
            Left(new Throwable("getResponseEncoding: Multiple `grpc-encoding` headers found"))
        }

    /** Side effecting stream that raises any errors in http response */
    protected def assertHeaderErrors(rep: Response[F]): Stream[F, Unit] = {
      import cats.syntax.alternative._
      import cats.syntax.semigroup._
      import cats.syntax.traverse._
      import cats.instances.option._
      val S = Stream.monadErrorInstance[F]
      import S.whenA

      def parseErrorHeaders(h: Headers): Stream[F, Unit] =
        Stream raiseError {
          new Exception(
            List(
              h.get(CIString("grpc-status")),
              h.get(CIString("grpc-message"))
            ).unite.map(_.toString) mkString "\n"
          )
        }

      for {
        trailerHeaders <- Stream eval rep.trailerHeaders
        headers = rep.headers combine trailerHeaders
        _ <- whenA(rep.status != Status.Ok)(parseErrorHeaders(headers))
        _ <- whenA(
          headers
            .get(CIString("grpc-status"))
            .traverse(_.toList)
            .unite
            .exists(_.value.toInt != 0)
        )(parseErrorHeaders(headers))
      } yield ()
    }
  }

  /** Used to encode the GRPC payload.
    * @see
    *   [[https://github.com/grpc/grpc/blob/master/doc/PROTOCOL-HTTP2.md Length-Prefixed-Message]]
    *   term.
    */
  protected def lengthPrefixedMessage[A](
      encoding: ContentCoding,
      message: Codec[A]
  ): Codec[A] = {
    import scodec.Err
    import scodec.codecs.{uint32, variableSizeBytesLong, fail}
    import scodec.codecs.literals.constantIntCodec
    if (encoding matches ContentCoding.identity)
      0x00 ~> variableSizeBytesLong(uint32, message)
    else
      fail(Err(s"lengthPrefixedMessage: currently $encoding not supported"))
  }
}
